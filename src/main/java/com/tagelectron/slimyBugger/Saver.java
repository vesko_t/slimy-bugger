package com.tagelectron.slimyBugger;

import com.tagelectron.engine.events.EventHandler;
import com.tagelectron.engine.events.EventManager;
import com.tagelectron.engine.events.EventType;
import com.tagelectron.engine.world.TiledMap;
import com.tagelectron.slimyBugger.entities.CustomWorld;
import com.tagelectron.slimyBugger.entities.Player;
import com.tagelectron.slimyBugger.items.Inventory;
import com.tagelectron.slimyBugger.items.Item;
import com.tagelectron.slimyBugger.items.ItemType;
import com.tagelectron.slimyBugger.items.equipment.Equipable;
import com.tagelectron.slimyBugger.items.equipment.Weapon;
import org.apache.commons.io.FileUtils;
import org.joml.Vector2f;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by Vesko on 15.11.2018 г..
 */
public class Saver implements EventHandler {

    private static Document document;
    private static File saveFile;
    private static Element playerMap;
    private static Vector2f playerPosition;
    private static int playerHealth;
    private static List<String> savedMaps = new ArrayList<>();

    public Saver(File saveFile) {
        EventManager.getEventHandlers().add(this);
        Saver.saveFile = saveFile;
        if (saveFile.exists() && !decompress(getFileContents()).equals("")) {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);
            DocumentBuilder builder = null;
            try {
                builder = factory.newDocumentBuilder();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
            if (builder != null) {
                try {
                    document = builder.parse(new InputSource(new StringReader(decompress(getFileContents()))));
                    if (document.getElementsByTagName("save").item(0) == null) {
                        Element saveElement = document.createElement("save");
                        document.appendChild(saveElement);
                        Element playerElement = document.createElement("player");
                        saveElement.appendChild(playerElement);
                    }
                } catch (SAXException e) {
                    playerPosition = null;
                    playerMap = null;
                } catch (IOException ignored) {

                }
            } else return;
        } else {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);
            DocumentBuilder builder = null;
            try {
                builder = factory.newDocumentBuilder();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
            if (builder != null) {
                document = builder.newDocument();
                Element saveElement = document.createElement("save");
                document.appendChild(saveElement);
                Element playerElement = document.createElement("player");
                saveElement.appendChild(playerElement);
            } else return;
        }
        updateSaver();
    }

    public static void updateSaver() {
        Element saveElement = (Element) document.getElementsByTagName("save").item(0);
        Element playerElement = (Element) saveElement.getElementsByTagName("player").item(0);
        Element mapsElement = (Element) saveElement.getElementsByTagName("maps").item(0);
        if (playerElement != null) {
            if (playerElement.hasAttributes()) {
                float xPos = Float.parseFloat(playerElement.getAttribute("xPos"));
                float yPos = Float.parseFloat(playerElement.getAttribute("yPos"));
                playerPosition = new Vector2f(xPos, yPos);
                playerHealth = Integer.parseInt(playerElement.getAttribute("health"));
                playerMap = (Element) ((Element) playerElement.getElementsByTagName("player-map").item(0)).getElementsByTagName("map").item(0);
            }
        } else {
            playerElement = document.createElement("player");
            saveElement.appendChild(playerElement);
        }
        if (mapsElement != null) {
            NodeList maps = mapsElement.getElementsByTagName("savedMap");
            for (int i = 0; i < maps.getLength(); i++) {
                Element map = (Element) maps.item(i);
                String savedMap = map.getAttribute("name");
                savedMaps.add(savedMap);
            }
        }
    }

    @Override
    public void onEvent(EventType eventType) {
        if (eventType.equals(EventType.Save)) {
            save();
        }
    }

    public static Element getSavedMap(String savedMapName) {
        Element saveElement = (Element) document.getElementsByTagName("save").item(0);
        NodeList maps = ((Element) saveElement.getElementsByTagName("maps").item(0)).getElementsByTagName("savedMap");
        for (int i = 0; i < maps.getLength(); i++) {
            Element map = (Element) maps.item(i);
            if (map.getAttribute("name").equals(savedMapName)) {
                return (Element) map.getElementsByTagName("map").item(0);
            }
        }
        return null;
    }

    public void save() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Saver.savePlayer(CustomWorld.getPlayer());
                if (CustomWorld.getCurrentMap().getType().equals("overworld")) {
                    Saver.saveMap(CustomWorld.getCurrentMap());
                }
                Saver.writeToFile();
                Saver.updateSaver();
            }
        });
        thread.start();
    }

    public static void savePlayer(Player player) {
        Element playerElement = (Element) ((Element) document.getElementsByTagName("save").item(0)).getElementsByTagName("player").item(0);
        playerElement.setAttribute("xPos", String.valueOf(player.getPosition().x));
        playerElement.setAttribute("yPos", String.valueOf(player.getPosition().y));
        playerElement.setAttribute("health", String.valueOf(CustomWorld.getPlayerHealth()));
        Inventory inventory = player.getInventory();
        Element inventoryElement = document.createElement("inventory");
        inventory.getItems().forEach((key, value) -> {
            value.forEach(items -> {
                items.forEach(item -> {
                    if (item instanceof Equipable) {
                        Equipable equipable = (Equipable) item;
                        Element gearElement = document.createElement("gear");
                        gearElement.setAttribute("name", equipable.getName());
                        gearElement.setAttribute("level", String.valueOf(equipable.getLevel()));
                        gearElement.setAttribute("type", equipable.getType().toString());
                        inventoryElement.appendChild(gearElement);
                    } else {
                        Element itemElement = document.createElement(item.getType().toString());
                        itemElement.setAttribute("name", item.getName());
                        itemElement.setAttribute("type", item.getType().toString());
                        inventoryElement.appendChild(itemElement);
                    }
                });
            });
        });
        Element playerMap = (Element) playerElement.getElementsByTagName("player-map").item(0);
        if (playerMap != null) {
            playerElement.removeChild(playerMap);
        }
        playerMap = document.createElement("player-map");
        playerElement.appendChild(playerMap);
        Element currentMapElement = (Element) CustomWorld.getCurrentMap().getMapDocument().cloneNode(true);
        document.adoptNode(currentMapElement);
        playerMap.appendChild(currentMapElement);
        playerMap.appendChild(currentMapElement);

    }

    public static int getPlayerHealth() {
        return playerHealth;
    }

    public static void cleanUp() {
        writeToFile();
    }

    public static void saveMap(TiledMap tiledMap) {
        Element saveElement = (Element) document.getElementsByTagName("save").item(0);
        Element mapsElement = (Element) saveElement.getElementsByTagName("maps").item(0);
        if (mapsElement == null) {
            mapsElement = document.createElement("maps");
            saveElement.appendChild(mapsElement);
        }
        NodeList mapList = mapsElement.getElementsByTagName("savedMap");

        for (int i = 0; i < mapList.getLength(); i++) {
            Element map = (Element) mapList.item(i);
            if (map.getAttribute("name").equals(tiledMap.getName())) {
                map.getParentNode().removeChild(map);
            }
        }
        Element mapElement = document.createElement("savedMap");
        mapElement.setAttribute("name", tiledMap.getName());
        Element currentMapElement = (Element) tiledMap.getMapDocument().cloneNode(true);
        document.adoptNode(currentMapElement);
        mapElement.appendChild(currentMapElement);
        mapsElement.appendChild(mapElement);
    }

    public static void writeToFile() {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        if (!saveFile.exists()) {
            try {
                Path path = Paths.get("./saves/");
                System.out.println(path.toFile().exists());
                if (!Files.exists(path)) {
                    Files.createDirectory(path);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            Transformer transformer = transformerFactory.newTransformer();

            DOMSource domSource = new DOMSource(document);

            StringWriter stringWriter = new StringWriter();

            StreamResult streamResult = new StreamResult(stringWriter);

            transformer.transform(domSource, streamResult);

            saveStringToFile(compressString(stringWriter.toString()));


        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    public static byte[] compressString(String string) {
        if (string == null || string.length() == 0) {
            return null;
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            GZIPOutputStream gzip = new GZIPOutputStream(out);
            gzip.write(string.getBytes(StandardCharsets.UTF_8));
            gzip.close();
            return out.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void saveStringToFile(byte[] bytes) {
        try {
            FileUtils.writeByteArrayToFile(saveFile, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static byte[] getFileContents() {
        try {
            return FileUtils.readFileToByteArray(saveFile);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String decompress(byte[] bytes) {
        ByteArrayInputStream in = new ByteArrayInputStream(bytes);
        try {
            GZIPInputStream gzip = new GZIPInputStream(in);
            BufferedReader br = new BufferedReader(new InputStreamReader(gzip, "UTF-8"));
            StringBuilder sb = new StringBuilder();
            br.lines().forEach((line) -> {
                sb.append(line);
                sb.append("\n");
            });
            gzip.close();
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Element getPlayerMap() {
        return playerMap;
    }

    public static Vector2f getPlayerPosition() {
        return playerPosition;
    }

    public static List<String> getSavedMaps() {
        return savedMaps;
    }
}
