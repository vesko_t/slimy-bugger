package com.tagelectron.slimyBugger.entities;

import com.tagelectron.engine.audio.AudioManager;
import com.tagelectron.engine.audio.AudioSource;
import com.tagelectron.engine.collision.Collidable;
import com.tagelectron.engine.collision.Collision;
import com.tagelectron.engine.events.EventType;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.animations.ColorAnimation;
import com.tagelectron.engine.graphics.animations.TextureAnimation;
import com.tagelectron.engine.graphics.gui.GuiBar;
import com.tagelectron.engine.rendering.Camera;
import com.tagelectron.engine.rendering.Renderer;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.text.GLText;
import com.tagelectron.engine.text.GLTrueTypeFont;
import com.tagelectron.engine.world.Layer;
import com.tagelectron.engine.world.TexturedTile;
import com.tagelectron.engine.world.Tile;
import com.tagelectron.engine.world.World;
import com.tagelectron.slimyBugger.CustomUpdater;
import com.tagelectron.slimyBugger.animations.AnimationType;
import com.tagelectron.slimyBugger.items.Inventory;
import com.tagelectron.slimyBugger.items.Item;
import com.tagelectron.slimyBugger.items.ItemType;
import com.tagelectron.slimyBugger.rendering.CustomRenderer;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;
import org.joml.Vector4f;

import java.util.ArrayList;
import java.util.List;

public class Enemy extends Entity {

    /*http://gaurav.munjal.us/Universal-LPC-Spritesheet-Character-Generator/#?body=orc&sex=male&eyes=green&armor=chest_plate&hair=none&arms=plate&greaves=metal&gloves=metal&hat=helmet_metal&shoes=boots_metal&weapon=spear&shield=on*/

    private List<Node> path;

    private Node currentGoal;

    private final int id;

    private Texture itemTexture = new Texture("/images/tile2.png");

    private GLTrueTypeFont trueTypeFont = new GLTrueTypeFont("/fonts/arial.ttf", 20);
    private GLTrueTypeFont criticalFont = new GLTrueTypeFont("/fonts/arial.ttf", 25);
    private List<GLText> damageTexts = new ArrayList<>();

    private boolean hasAgro = false;

    private List<Class> targets = new ArrayList<>();
    private float attackDistance = 400;

    public Enemy(Vector2f position, Shader shader, Vector2f scale, GuiBar bar, Texture sprite, int health, float movementSpeed, int id) {
        super(position, shader, scale, "CollisionLayer", new Vector2i(5), movementSpeed, bar, health, 6);
        this.id = id;
        loadAnimations(sprite);
        animation = animations.get(AnimationType.IdleDown);
        isFacingDown = true;
        terrainBox.setScale(new Vector2f(22, 30));
        entityBox.setScale(new Vector2f(22, 22));
        audioSource = new AudioSource();
        stepsSound = AudioManager.loadSound("/sounds/step_metal.ogg");
        audioSource.setLooping(false);

        swooshSource = new AudioSource();
        swooshSource.setGain(0.5f);
        swooshSource.setPitch(1.8f);
        swooshSound = AudioManager.loadSound("/sounds/swoosh.ogg");
        swooshSource.setLooping(false);

        targets.add(Player.class);

    }

    private void loadAnimations(Texture texture) {
        animations.put(AnimationType.WalkUp, new TextureAnimation(texture, 9, 21, 13, 8, 10, this));
        animations.put(AnimationType.WalkDown, new TextureAnimation(texture, 9, 21, 13, 10, 10, this));
        animations.put(AnimationType.WalkLeft, new TextureAnimation(texture, 9, 21, 13, 9, 10, this));
        animations.put(AnimationType.WalkRight, new TextureAnimation(texture, 9, 21, 13, 11, 10, this));

        animations.put(AnimationType.IdleUp, new TextureAnimation(texture, 1, 21, 13, 8, 1, this));
        animations.put(AnimationType.IdleDown, new TextureAnimation(texture, 1, 21, 13, 10, 1, this));
        animations.put(AnimationType.IdleLeft, new TextureAnimation(texture, 1, 21, 13, 9, 1, this));
        animations.put(AnimationType.IdleRight, new TextureAnimation(texture, 1, 21, 13, 11, 1, this));

        animations.put(AnimationType.AttackUp, new TextureAnimation(texture, 8, 21, 13, 4, 30, this, 1));
        animations.put(AnimationType.AttackDown, new TextureAnimation(texture, 8, 21, 13, 6, 30, this, 1));
        animations.put(AnimationType.AttackLeft, new TextureAnimation(texture, 8, 21, 13, 5, 30, this, 1));
        animations.put(AnimationType.AttackRight, new TextureAnimation(texture, 8, 21, 13, 7, 30, this, 1));

        animations.put(AnimationType.HitColor, new ColorAnimation(15, shader, 1, new Vector4f(1, 0, 0, 1), new Vector4f(1, 1, 1, 1)));
    }

    @Override
    protected void onCollision(Tile tile, Collision collision) {

    }

    @Override
    protected void onCollision(Collidable collidable, Collision collision) {
    }

    public void update(float v) {
        if (CustomUpdater.getTotalTicks() % 60 == 0) {
            calculatePath();
        }
        if (path != null) {
            followPath(v);
        } else velocity = new Vector2f(0);
        updateCollision();
        if (target != null) {
            updateAttack();
        }
        updateAnimation();
        if (health <= 0) {
            onDeath();
            toBeRemoved = true;
            CustomWorld.enemyDeath(id);
        }
        for (int i = 0; i < damageTexts.size(); i++) {
            GLText glText = damageTexts.get(i);
            Vector2f screenPos = Camera.getPositionScreen(position);
            glText.setPosition(new Vector2f(screenPos.x - glText.getLength() / 2, screenPos.y - (4.0f / (glText.getOpacity() / 4.0f))));
            glText.setOpacity(glText.getOpacity() - 0.01f);
        }
    }

    private void updateAttack() {
        float distance = position.distance(target.getPosition());
        if (distance > attackDistance) {
            velocity = new Vector2f(0);
            target.setInCombat(false);
            target = null;
            super.isMoving = false;
            return;
        } else {
            updateVelocity();
        }
        if (distance < scale.x + target.getScale().x + 10) {
            attack();
            hasAgro = true;
        }
    }

    private void onDeath() {
        Item item = new Item(itemTexture, ItemType.Food, "Random Item", "Description of random item");
        item.drop(position);
        if (target != null) {
            target.setInCombat(false);
        }
    }

    @Override
    public void onEvent(EventType eventType) {

    }

    @Override
    protected void checkEntityAround(Collidable collidable) {
        targets.forEach(aClass -> {
            if (collidable.getClass().equals(aClass) && !collidable.equals(this)) {
                if (target == null) {
                    float distance = Math.abs(collidable.getPosition().x - position.x) + Math.abs(collidable.getPosition().y - position.y);
                    if (distance < attackDistance) {
                        target = (Entity) collidable;
                    }
                }
            }
        });
    }

    @Override
    public void hit(int damage, boolean isCritical, Entity attacker) {
        super.hit(damage, isCritical, attacker);
        GLText glText = new GLText(isCritical ? criticalFont : trueTypeFont, String.valueOf(damage), new Vector2f(), CustomRenderer.getTextShader(), isCritical ? new Vector3f(1, 1, 0) : new Vector3f(1, 1, 1));
        Vector2f screenPos = Camera.getPositionScreen(position);
        glText.setPosition(screenPos);
        damageTexts.add(glText);
        target = attacker;
        target.setInCombat(true);
    }

    private void attack() {
        if (CustomUpdater.getTotalTicks() % 30 == 0) {
            Vector2f distance = new Vector2f();
            target.setInCombat(true);
            target.getPosition().sub(position, distance);
            setAttackingAnimation((float) Math.toDegrees(new Vector2f(2, 0).angle(distance)));
        }
    }

    private void calculatePath() {
        if (target == null) {
            return;
        }
        Layer layer = World.getCurrentMap().getLayers().get(collisionLayerName);
        List<Node> openList = new ArrayList<Node>();
        List<Node> closeList = new ArrayList<Node>();
        Node start = new Node(layer.getTile(World.getWorldPosition(position)));
        Node end = new Node(layer.getTile(World.getWorldPosition(target.getPosition())));
        start.setParent(null);
        start.setMovementCost(0);
        start.setTotalScore(start.getMovementCost() + heuristic(start, end));
        openList.add(start);
        while (!openList.isEmpty()) {
            Node current = getLowestScore(openList);
            if (current.equals(end)) {
                path = constructPath(current);
                return;
            }
            openList.remove(current);
            closeList.add(current);
            for (Node neighbor : getNeighbors(current, layer)) {
                if (!closeList.contains(neighbor)) {
                    float movementCost;
                    if (neighbor.getWordPosition().y != current.getWordPosition().y && neighbor.getWordPosition().x != current.getWordPosition().x) {
                        movementCost = current.getMovementCost() + World.getDiagonalTileDistance();
                    } else {
                        movementCost = current.getMovementCost() + World.getHorizontalTileDistance();
                    }
                    neighbor.setTotalScore(movementCost + heuristic(neighbor, end));
                    neighbor.setParent(current);
                    neighbor.setMovementCost(movementCost);
                    if (!openList.contains(neighbor)) {
                        openList.add(neighbor);
                    }
                }
            }
        }
        path = null;
    }

    private void followPath(float delta) {
        if (World.getWorldPosition(position).equals(currentGoal.getWordPosition())) {
            if (path.indexOf(currentGoal) - 1 >= 0) {
                currentGoal = path.get(path.indexOf(currentGoal) - 1);
            }
        }
        currentGoal.getPosition().sub(position, velocity);
        velocity.normalize(movementConst * delta);
    }

    private List<Node> getNeighbors(Node tile, Layer layer) {
        List<Tile> tiles = layer.getTilesAround(tile.getWordPosition(), new Vector2i(1));
        List<Tile> invalidTiles = new ArrayList<>();
        List<Node> nodes = new ArrayList<>();
        for (Tile neighbour : tiles) {
            if (neighbour instanceof TexturedTile || neighbour.equals(tile)) {
                invalidTiles.add(neighbour);
            } else {
                nodes.add(new Node(neighbour));
            }
        }
        tiles.removeAll(invalidTiles);
        return nodes;
    }

    private Node getLowestScore(List<Node> tiles) {
        Node tileToReturn = null;
        float lowest = Float.MAX_VALUE;
        for (Node tile : tiles) {
            if (tile.getTotalScore() < lowest) {
                lowest = tile.getTotalScore();
                tileToReturn = tile;
            }
        }
        return tileToReturn;
    }

    private float heuristic(Node start, Node end) {
        float dMax = Math.max(Math.abs(start.getWordPosition().x - end.getWordPosition().x), Math.abs(start.getWordPosition().y - end.getWordPosition().y));
        float dMin = Math.min(Math.abs(start.getWordPosition().x - end.getWordPosition().x), Math.abs(start.getWordPosition().y - end.getWordPosition().y));

        return World.getDiagonalTileDistance() * dMin + World.getHorizontalTileDistance() * (dMax - dMin);
    }

    private List<Node> constructPath(Node endNode) {
        List<Node> path = new ArrayList<Node>();
        path.add(endNode);
        int length = 1;
        while (endNode.getParent() != null) {
            endNode = endNode.getParent();
            path.add(endNode);
            length++;
        }
        currentGoal = path.get(length - 1);
        return path;
    }

    @Override
    protected void onAttack(Entity target) {
        target.hit(20, false, this);
    }

    @Override
    public void constantUpdate(float v) {
        if (CustomUpdater.isPaused()) {
            audioSource.stop();
        }
    }

    @Override
    public void render() {
        super.render();
        List<GLText> toRemove = new ArrayList<>();
        for (int i = 0; i < damageTexts.size(); i++) {
            GLText glText = damageTexts.get(i);
            if (glText.getOpacity() < 0) {
                toRemove.add(glText);
            } else {
                glText.render();
                //System.out.println("Text: "+glText.getText());
            }
        }
        damageTexts.removeAll(toRemove);
    }
}
