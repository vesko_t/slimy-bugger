package com.tagelectron.slimyBugger.entities;

import com.tagelectron.engine.world.Tile;
import com.tagelectron.engine.world.World;
import org.joml.Vector2f;
import org.joml.Vector2i;

/**
 * Created by vesko on 23.11.2018 г..
 */
public class Node {
    private Node parent;
    private Vector2f position;
    private float movementCost;
    private float totalScore;
    private Vector2i worldPosition;

    public Node(Tile tile) {
        position = new Vector2f(tile.getPosition());
        worldPosition = tile.getWordPosition();
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public Vector2f getPosition() {
        return position;
    }

    public void setPosition(Vector2f position) {
        this.position = position;
    }

    public float getMovementCost() {
        return movementCost;
    }

    public void setMovementCost(float movementCost) {
        this.movementCost = movementCost;
    }

    public float getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(float totalScore) {
        this.totalScore = totalScore;
    }

    public Vector2i getWordPosition() {
        return worldPosition;
    }

    public void setWorldPosition(Vector2i worldPosition) {
        this.worldPosition = worldPosition;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) || ((Node) obj).getPosition().equals(position);
    }
}
