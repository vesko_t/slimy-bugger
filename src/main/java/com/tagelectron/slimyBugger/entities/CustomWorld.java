package com.tagelectron.slimyBugger.entities;

import com.tagelectron.engine.entities.Updater;
import com.tagelectron.engine.events.EventManager;
import com.tagelectron.engine.events.EventType;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.gui.GuiBar;
import com.tagelectron.engine.graphics.gui.GuiElement;
import com.tagelectron.engine.graphics.gui.GuiManager;
import com.tagelectron.engine.rendering.Renderable;
import com.tagelectron.engine.rendering.Renderer;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.text.GLText;
import com.tagelectron.engine.text.GLTrueTypeFont;
import com.tagelectron.engine.world.MapObject;
import com.tagelectron.engine.world.TiledMap;
import com.tagelectron.engine.world.World;
import com.tagelectron.slimyBugger.Saver;
import com.tagelectron.slimyBugger.gui.MovableComplexGui;
import com.tagelectron.slimyBugger.gui.MovableGuiTextBox;
import com.tagelectron.slimyBugger.items.*;
import com.tagelectron.slimyBugger.items.drops.Container;
import com.tagelectron.slimyBugger.items.utils.ItemManager;
import com.tagelectron.slimyBugger.rendering.CustomRenderer;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Vesko on 29.10.2018 г..
 */
public class CustomWorld extends World {

    private static final Shader playerShader = new Shader("player");
    private static final Shader npcShader = new Shader("npc");
    private static Player player;
    private static GLTrueTypeFont font;
    private static GLTrueTypeFont biggerFont;
    private static final Texture guiTexture = new Texture("/images/gui-box-low-res.png");
    private static int playerHealth = 1000;
    private static Vector2f playerPosition;
    private static Map<String, EntityStats> entityStats;
    private static boolean loadsNew;
    private static Inventory inventory;

    public CustomWorld(Shader shader, Vector3f scale, Map<String, EntityStats> entityStats) {
        super(shader, scale);
        CustomWorld.entityStats = entityStats;
        font = new GLTrueTypeFont("/fonts/arial.ttf", 12);
        biggerFont = new GLTrueTypeFont("/fonts/arial.ttf", 15);
    }

    public CustomWorld(String mapName, Shader shader, Vector3f scale, Map<String, EntityStats> entityStats) {
        super(mapName, shader, scale);
        CustomWorld.entityStats = entityStats;
        font = new GLTrueTypeFont("/fonts/arial.ttf", 12);
        biggerFont = new GLTrueTypeFont("/fonts/arial.ttf", 15);
        currentMap = loadMap(mapName, scale);
        afterMapLoad(currentMap);
    }


    private void loadMapInformation(TiledMap tiledMap) {
        Texture containerTexture = new Texture("/images/tile2.png");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        try {
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            Document document = documentBuilder.parse(CustomWorld.class.getResourceAsStream("/entity-config/container.xml"));
            Element mapElement = null;
            for (int i = 0; i < document.getElementsByTagName("containers").getLength(); i++) {
                Element containersElement = (Element) document.getElementsByTagName("containers").item(i);
                if (containersElement.getAttribute("map").equals(tiledMap.getName())) {
                    mapElement = containersElement;
                }

            }
            if (mapElement == null) {
                return;
            }
            for (MapObject mapObject : tiledMap.getObject("container")) {
                for (int i = 0; i < mapElement.getElementsByTagName("container").getLength(); i++) {
                    Element container = (Element) mapElement.getElementsByTagName("container").item(i);
                    if (container.getAttribute("id").equals(mapObject.getType())) {
                        Container chest = new Container(containerTexture, mapObject.getPosition(), new Vector2f(32));
                        for (int j = 0; j < container.getElementsByTagName("item").getLength(); j++) {
                            Element itemElement = (Element) container.getElementsByTagName("item").item(j);
                            chest.getItems().add(ItemManager.getItems()
                                    .get(ItemType.valueOf(itemElement.getAttribute("type")))
                                    .get(itemElement.getAttribute("name")));
                        }
                        Renderer.getRenderables().add(chest);
                        Updater.getUpdateables().add(chest);
                    }
                }
            }
        } catch (SAXException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void afterMapLoad(TiledMap tiledMap) {
        EventManager.sendEvent(EventType.WorldLoading);
        Renderer.getRenderables().clear();
        Updater.getUpdateables().clear();
        GuiBar playerBar = new GuiBar(new Vector2f(0), new Vector2f(80, 18), new Vector4f(0, 1, 0, 1), true);

        loadMapInformation(tiledMap);

        player = new Player(
                playerPosition != null ? playerPosition : tiledMap.getObject("PlayerStart").get(0).getPosition(),
                playerShader,
                new Vector2f(120, 120),
                playerBar,
                playerHealth,
                inventory,
                "Scythe of Death",
                "Leather chest",
                "Metal Legs",
                "Leather Boots");
        playerPosition = null;


        GuiManager.getEntityGuis().clear();

        List<GuiElement> entityGuis = new ArrayList<>();
        List<Renderable> renderables = new ArrayList<>();

        if (tiledMap.getObject("EnemyStart") != null) {
            for (MapObject mapObject : tiledMap.getObject("EnemyStart")) {
                GuiBar bar = new GuiBar(new Vector2f(2, 15), new Vector2f(30, 3), new Vector4f(0, 1, 0, 1), true);
                EntityStats enemyStats = entityStats.get(mapObject.getType());
                Enemy movingEnemy = new Enemy(mapObject.getPosition(), npcShader, new Vector2f(32, 32), bar, enemyStats.getSprite(), enemyStats.getHealth(), enemyStats.getMovementSpeed(), mapObject.getId());
                MovableComplexGui complexGui = new MovableComplexGui(new Vector2f(0, 100), new Vector2f(32, 12), new GLText(font, mapObject.getProperties().get("CharacterName"), CustomRenderer.getTextShader(), new Vector3f(1)), guiTexture, true, movingEnemy);
                complexGui.addGui(bar);
                entityGuis.add(complexGui);
                renderables.add(movingEnemy);
            }
        }

        for (MapObject mapObject : tiledMap.getObject("NpcStart")) {
            MovableGuiTextBox textBox = new MovableGuiTextBox(new Vector2f(0, 100), new Vector2f(40, 15), new GLText(biggerFont, "Name", CustomRenderer.getTextShader(), new Vector3f(1)), guiTexture, true, new Vector2f(2));
            Npc npc = new Npc(mapObject.getPosition(), npcShader, new Vector2f(42),
                    new GuiBar(new Vector2f(2, 15), new Vector2f(30, 3),
                            new Vector4f(0, 1, 0, 1), true),
                    textBox, mapObject.getProperties().get("CharacterName"),
                    entityStats.get(mapObject.getType()).getDialogs());
            entityGuis.add(textBox);
            renderables.add(npc);
        }

        renderables.add(player);
        entityGuis.add(playerBar);
        Renderer.getRenderables().addAll(renderables);
        Updater.getUpdateables().addAll(renderables);
        GuiManager.getEntityGuis().addAll(entityGuis);
        EventManager.sendEvent(EventType.WorldLoaded);
    }

    public static void setPlayerPosition(Vector2f playerPosition) {
        CustomWorld.playerPosition = playerPosition;
    }

    @Override
    public void onEvent(EventType eventType) {
        if (eventType.equals(EventType.PlayerDeath)) {
            Renderer.getRenderables().clear();
            Updater.getUpdateables().clear();
            currentMap = null;
            player = null;
            playerHealth = 1000;
        } else if (eventType.equals(EventType.MapChange)) {
            loadsNew = false;
            if (Saver.getSavedMaps().contains(eventType.getCommand())) {
                mapElementToLoad = Saver.getSavedMap(eventType.getCommand());
            } else {
                mapName = eventType.getCommand();
            }
        } else if (eventType.equals(EventType.LoadNewMap)) {
            mapName = eventType.getCommand();
        } else if (eventType.equals(EventType.LoadLastSave)) {
            Renderer.getRenderables().clear();
            Updater.getUpdateables().clear();
            currentMap = null;
            player = null;
            playerPosition = Saver.getPlayerPosition();
            playerHealth = Saver.getPlayerHealth();
            mapElementToLoad = Saver.getPlayerMap();
        }
    }

    public static Player getPlayer() {
        return player;
    }

    public static int getPlayerHealth() {
        return playerHealth;
    }

    public static void setPlayerHealth(int playerHealth) {
        CustomWorld.playerHealth = playerHealth;
    }

    public static void enemyDeath(int enemyId) {
        if (currentMap.getType().equals("overworld")) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    TiledMap map = currentMap;
                    Element element = map.getMapDocument();

                    Element objectGroup = (Element) element.getElementsByTagName("objectgroup").item(0);

                    NodeList objectList = objectGroup.getElementsByTagName("object");

                    for (int i = 0; i < objectList.getLength(); i++) {
                        Element object = (Element) objectList.item(i);
                        int objectId = Integer.parseInt(object.getAttribute("id"));
                        if (objectId == enemyId) {
                            objectGroup.removeChild(object);
                        }
                    }
                    Saver.saveMap(map);
                }
            });
            thread.start();
        }
    }

    public static void setInventory(Inventory inventory) {
        CustomWorld.inventory = inventory;
    }
}
