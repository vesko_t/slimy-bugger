package com.tagelectron.slimyBugger.entities;

import com.tagelectron.engine.audio.AudioManager;
import com.tagelectron.engine.audio.AudioSource;
import com.tagelectron.engine.collision.Collidable;
import com.tagelectron.engine.collision.Collision;
import com.tagelectron.engine.entities.Updater;
import com.tagelectron.engine.events.EventManager;
import com.tagelectron.engine.events.EventType;
import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.animations.ColorAnimation;
import com.tagelectron.engine.graphics.animations.TextureAnimation;
import com.tagelectron.engine.graphics.gui.GuiBar;
import com.tagelectron.engine.graphics.projectiles.Projectile;
import com.tagelectron.engine.graphics.projectiles.Shooter;
import com.tagelectron.engine.io.Input;
import com.tagelectron.engine.rendering.Camera;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.world.MapObject;
import com.tagelectron.engine.world.Tile;
import com.tagelectron.engine.world.World;
import com.tagelectron.slimyBugger.CustomUpdater;
import com.tagelectron.slimyBugger.animations.AnimationManager;
import com.tagelectron.slimyBugger.animations.AnimationType;
import com.tagelectron.slimyBugger.animations.NamedMultipleTextureAnimation;
import com.tagelectron.slimyBugger.items.*;
import com.tagelectron.slimyBugger.items.equipment.Clothing;
import com.tagelectron.slimyBugger.items.equipment.Weapon;
import com.tagelectron.slimyBugger.items.equipment.WeaponType;
import com.tagelectron.slimyBugger.items.utils.ItemManager;
import com.tagelectron.slimyBugger.rendering.CustomRenderer;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;

import java.util.Map;
import java.util.Random;

public class Player extends Entity implements Shooter {
    private final Inventory inventory;

    private Weapon weapon;
    private Clothing legs;
    private Clothing chest;
    private Clothing boots;

    /*http://gaurav.munjal.us/Universal-LPC-Spritesheet-Character-Generator/#?sex=female&eyes=green&clothes=sleeveless_brown&legs=sara&shoes=ghillies&=shoes_black&cape=red&weapon=spear&hair=loose_brown&belt=leather&buckle=bronze&hat=none&gloves=none&greaves=none&bracers=leather&shoulders=leather&jacket=none&nose=none&ammo=none&quiver=none&capeacc=clip_red&bracelet=on*/

    public Player(Vector2f position, Shader shader, Vector2f scale, GuiBar bar, int health, Inventory inventory, String weapon, String chest, String legs, String boots) {
        super(position, shader, scale, "CollisionLayer", new Vector2i(5), 200, bar, 1000, 6);
        this.inventory = inventory;
        loadAnimations();
        setChest(chest);
        setLegs(legs);
        setBoots(boots);
        setWeapon(weapon);
        animation = animations.get(AnimationType.IdleDown);
        isFacingDown = true;
        terrainBox.setScale(new Vector2f(22, 42));
        entityBox.setScale(new Vector2f(22, 22));
        projectileModel = new Model(new float[]{
                1, 1,
                0, 1,
                0, 0,
                1, 0
        });
        projectile = new Projectile(new Texture("/images/fireball.png"), new Vector2f(position), projectileShader, new Vector2f(30, 10), projectileModel, "CollisionLayer", this, 600);
        projectile.setLightSource(true);
        audioSource = new AudioSource();
        stepsSound = AudioManager.loadSound("/sounds/step_lth1.ogg");
        audioSource.setLooping(false);

        swooshSource = new AudioSource();
        swooshSound = AudioManager.loadSound("/sounds/swoosh.ogg");
        swooshSource.setLooping(false);
        setHealth(health);
        Camera.setPosition(new Vector2f(-this.position.x, -this.position.y));
    }

    private void setBoots(String boots) {
        this.boots = (Clothing) ItemManager.getItems().get(ItemType.Clothes).get(boots);
        for (AnimationType animationType : AnimationType.values()) {
            if (!animationType.equals(AnimationType.HitColor)) {
                NamedMultipleTextureAnimation animation = (NamedMultipleTextureAnimation) animations.get(animationType);
                animation.getBoots().setSpriteSheet(this.boots.getSpriteSheet());
            }
        }
    }

    private void setLegs(String legs) {
        this.legs = (Clothing) ItemManager.getItems().get(ItemType.Clothes).get(legs);
        for (AnimationType animationType : AnimationType.values()) {
            if (!animationType.equals(AnimationType.HitColor)) {
                NamedMultipleTextureAnimation animation = (NamedMultipleTextureAnimation) animations.get(animationType);
                animation.getLegs().setSpriteSheet(this.legs.getSpriteSheet());
            }
        }
    }

    private void setChest(String chest) {
        this.chest = (Clothing) ItemManager.getItems().get(ItemType.Clothes).get(chest);
        for (AnimationType animationType : AnimationType.values()) {
            if (!animationType.equals(AnimationType.HitColor)) {
                NamedMultipleTextureAnimation animation = (NamedMultipleTextureAnimation) animations.get(animationType);
                animation.getChest().setSpriteSheet(this.chest.getSpriteSheet());
            }
        }
    }

    private void setWeapon(String weapon) {
        this.weapon = (Weapon) ItemManager.getItems().get(ItemType.Weapons).get(weapon);
        for (AnimationType animationType : AnimationType.values()) {
            if (!animationType.equals(AnimationType.HitColor)) {
                NamedMultipleTextureAnimation animation = (NamedMultipleTextureAnimation) animations.get(animationType);
                animation.getWeapon().setSpriteSheet(this.weapon.getSpriteSheet());
            }
        }
    }

    @Override
    protected void onCollision(Tile tile, Collision collision) {

    }

    @Override
    protected void onCollision(Collidable collidable, Collision collision) {

    }

    @Override
    public void onEvent(EventType eventType) {

    }

    @Override
    public void onHit(Collidable collidable) {
        if (collidable instanceof Enemy) {
            Random random = new Random();
            int criticalIndex = random.nextInt(4);
            if (criticalIndex == 1) {
                ((Enemy) collidable).hit(60, true, this);
            } else {
                ((Enemy) collidable).hit(30, false, this);
            }
        }
    }

    public void update(float v) {
        if (!busy) {
            if (Input.isKeyDown(GLFW.GLFW_KEY_W) && !Input.isKeyDown(GLFW.GLFW_KEY_S)) {
                velocity.y = movementConst * v;

            } else if (Input.isKeyDown(GLFW.GLFW_KEY_S) && !Input.isKeyDown(GLFW.GLFW_KEY_W)) {
                velocity.y = -movementConst * v;
            } else {
                velocity.y = 0;
            }

            if (Input.isKeyDown(GLFW.GLFW_KEY_D) && !Input.isKeyDown(GLFW.GLFW_KEY_A)) {
                velocity.x = movementConst * v;
            } else if (Input.isKeyDown(GLFW.GLFW_KEY_A) && !Input.isKeyDown(GLFW.GLFW_KEY_D)) {
                velocity.x = -movementConst * v;
            } else {
                velocity.x = 0;
            }
        }

        for (MapObject exit : World.getCurrentMap().getObjectByType("Exit")) {
            if (position.x - entityBox.getScale().x < exit.getPosition().x + exit.getScale().x &&
                    position.x + entityBox.getScale().x > exit.getPosition().x - exit.getScale().x &&
                    position.y - entityBox.getScale().y < exit.getPosition().y + exit.getScale().y &&
                    position.y + entityBox.getScale().y > exit.getPosition().y - exit.getScale().y) {
                EventManager.sendEvent(EventType.Save);

                EventType eventType = EventType.MapChange;
                eventType.setCommand(exit.getName());
                EventManager.sendEvent(eventType);
            }
        }

        if (Input.isMouseButtonPressed(1) && !Input.isMouseOverGui()) {
            float angle = -(Input.getMousePositionCamera().angle(new Vector2f(Camera.getWidth() / 2.0f, 0)));
            projectile.setPosition(new Vector2f(position));
            Projectile p = new Projectile(projectile);
            p.setTarget(Input.getMousePositionWorld());
            p.setRotation(angle);
            CustomRenderer.getRenderables().add(p);
            Updater.getUpdateables().add(p);
        }
        if (health <= 0) {
            EventManager.sendEvent(EventType.PlayerDeath);
        }


        updateCollision();
        audioSource.setVelocity(velocity);
        audioSource.setPosition(position);
        AudioManager.setListenerPosition(position);
        AudioManager.setListenerVelocity(velocity);
        if (animation != null) {
            if (!animation.equals(animations.get(AnimationType.Sheath)) || (animation.equals(animations.get(AnimationType.Sheath)) && !animation.isRunning())) {
                updateVelocity();
                updateAnimation();
            }
        }
        updateAttack();
    }

    public void updateAttack() {
        if (Input.isMouseButtonPressed(0)) {
            setAttackingAnimation(
                    (float) Math.toDegrees(new Vector2f(Camera.getWidth() / 2.0f, 0).angle(Input.getMousePositionCamera())));
        }
    }

    @Override
    protected void updateAnimation() {
        if (attacking && attackingAnimation != null) {
            if (!animation.equals(attackingAnimation)) {
                animation.stop();
                animation = attackingAnimation;
                animation.start();
                swooshSource.start(swooshSound);
                return;
            } else if (animation.isRunning()) {
                if (animation.getPointer() == attackFrame - 1) {
                    if (target != null) {
                        onAttack(target);
                    }
                }
                return;
            } else {
                attacking = false;
                animation.stop();
                attackingAnimation.stop();
            }

        }

        if (isMovingUp && !isMovingRight && !isMovingLeft) {
            if (!inCombat && weapon.getWeaponType().equals(WeaponType.Sword)) {
                if (!animation.equals(animations.get(AnimationType.WalkOutUp))) {
                    animation.stop();
                    animation = animations.get(AnimationType.WalkOutUp);
                    animation.start();
                }
            } else {
                if (!animation.equals(animations.get(AnimationType.WalkUp))) {
                    animation.stop();
                    animation = animations.get(AnimationType.WalkUp);
                    animation.start();
                }
            }
        } else if (isMovingDown && !isMovingRight && !isMovingLeft) {
            if (!inCombat && weapon.getWeaponType().equals(WeaponType.Sword)) {
                if (!animation.equals(animations.get(AnimationType.WalkOutDown))) {
                    animation.stop();
                    animation = animations.get(AnimationType.WalkOutDown);
                    animation.start();
                }
            } else {
                if (!animation.equals(animations.get(AnimationType.WalkDown))) {
                    animation.stop();
                    animation = animations.get(AnimationType.WalkDown);
                    animation.start();
                }
            }
        }

        if (isMovingRight) {
            if (!inCombat && weapon.getWeaponType().equals(WeaponType.Sword)) {
                if (!animation.equals(animations.get(AnimationType.WalkOutRight))) {
                    animation.stop();
                    animation = animations.get(AnimationType.WalkOutRight);
                    animation.start();
                }
            } else {
                if (!animation.equals(animations.get(AnimationType.WalkRight))) {
                    animation.stop();
                    animation = animations.get(AnimationType.WalkRight);
                    animation.start();
                }
            }
        } else if (isMovingLeft) {
            if (!inCombat && weapon.getWeaponType().equals(WeaponType.Sword)) {
                if (!animation.equals(animations.get(AnimationType.WalkOutLeft))) {
                    animation.stop();
                    animation = animations.get(AnimationType.WalkOutLeft);
                    animation.start();
                }
            } else {
                if (!animation.equals(animations.get(AnimationType.WalkLeft))) {
                    animation.stop();
                    animation = animations.get(AnimationType.WalkLeft);
                    animation.start();
                }
            }
        }

        if (!isMoving || CustomUpdater.isPaused()) {
            audioSource.stop();
            if (isFacingUp) {
                animation.stop();
                if (!inCombat) {
                    animation = animations.get(AnimationType.IdleUp);
                } else animation = animations.get(AnimationType.IdleOutUp);
                animation.start();
            } else if (isFacingDown) {
                animation.stop();
                if (!inCombat) {
                    animation = animations.get(AnimationType.IdleDown);
                } else animation = animations.get(AnimationType.IdleOutDown);
                animation.start();
            }

            if (isFacingLeft) {
                animation.stop();
                if (!inCombat) {
                    animation = animations.get(AnimationType.IdleLeft);
                } else animation = animations.get(AnimationType.IdleOutLeft);
                animation.start();
            } else if (isFacingRight) {
                animation.stop();
                if (!inCombat) {
                    animation = animations.get(AnimationType.IdleRight);
                } else animation = animations.get(AnimationType.IdleOutRight);
                animation.start();
            }
        } else {
            if (!audioSource.isPlaying() && CustomUpdater.getTotalTicks() % 26 == 0) {
                audioSource.start(stepsSound);
            }
        }
    }

    @Override
    public void constantUpdate(float v) {
        if (CustomUpdater.isPaused()) {
            audioSource.stop();
        }
    }

    @Override
    public void hit(int damage, boolean isCritical, Entity attacker) {
        super.hit(damage, isCritical, attacker);
        CustomWorld.setPlayerHealth(health);
    }

    @Override
    public void setPosition(Vector2f position) {
        super.setPosition(position);
        Camera.setPosition(new Vector2f(-this.position.x, -this.position.y));
    }

    @Override
    protected void checkEntityAround(Collidable collidable) {
        if (attacking) {
            if (collidable instanceof Enemy) {
                float distance = position.distance(collidable.getPosition());
                if (distance < (scale.x + collidable.getScale().x)) {
                    target = (Entity) collidable;
                }
            }
        }
    }

    private void loadAnimations() {
        Texture walkTexture = new Texture("/images/sprites/base-character.png");
        weapon = (Weapon) ItemManager.getItems().get(ItemType.Weapons).get("Lightning Staff");
        chest = (Clothing) ItemManager.getItems().get(ItemType.Clothes).get("Leather chest");
        legs = (Clothing) ItemManager.getItems().get(ItemType.Clothes).get("Metal Legs");
        boots = (Clothing) ItemManager.getItems().get(ItemType.Clothes).get("Leather Boots");

        Map<AnimationType, TextureAnimation> baseAnimations = AnimationManager.getAnimations(walkTexture, this);
        Map<AnimationType, TextureAnimation> swordAnimations = AnimationManager.getAnimations(weapon.getSpriteSheet(), this);
        Map<AnimationType, TextureAnimation> legsAnimation = AnimationManager.getAnimations(legs.getSpriteSheet(), this);
        Map<AnimationType, TextureAnimation> chestAnimation = AnimationManager.getAnimations(chest.getSpriteSheet(), this);
        Map<AnimationType, TextureAnimation> bootsAnimation = AnimationManager.getAnimations(boots.getSpriteSheet(), this);
        for (AnimationType animationType : AnimationType.values()) {
            if (!animationType.equals(AnimationType.HitColor)) {
                NamedMultipleTextureAnimation textureAnimation = new NamedMultipleTextureAnimation(baseAnimations.get(animationType));
                textureAnimation.setChest(chestAnimation.get(animationType));
                textureAnimation.setLegs(legsAnimation.get(animationType));
                textureAnimation.setBoots(bootsAnimation.get(animationType));
                textureAnimation.setWeapon(swordAnimations.get(animationType));
                animations.put(animationType, textureAnimation);
            }
        }
        animations.put(AnimationType.HitColor, new ColorAnimation(15, shader, 1, new Vector4f(1, 0, 0, 1), new Vector4f(1, 1, 1, 1)));
    }

    @Override
    protected void setAttackingAnimation(float angle) {
        if (inCombat) {
            attacking = true;
            if (!isMoving) {
                super.setAttackingAnimation(angle);
            } else {
                if (isMovingLeft) {
                    attackingAnimation = animations.get(AnimationType.AttackWalkLeft);
                } else if (isMovingRight) {
                    attackingAnimation = animations.get(AnimationType.AttackWalkRight);
                } else if (isMovingUp) {
                    attackingAnimation = animations.get(AnimationType.AttackWalkUp);
                } else if (isMovingDown) {
                    attackingAnimation = animations.get(AnimationType.AttackWalkDown);
                }
            }
        }
    }

    @Override
    protected void onAttack(Entity target) {
        weapon.attack(this, target);
    }

    public Inventory getInventory() {
        return inventory;
    }

    @Override
    public void setInCombat(boolean inCombat) {
        if (!inCombat && weapon.getWeaponType().equals(WeaponType.Sword)) {
            this.animation = animations.get(AnimationType.Sheath);
            animation.start();
            attacking = false;
        }
        super.setInCombat(inCombat);
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public Clothing getLegs() {
        return legs;
    }

    public Clothing getChest() {
        return chest;
    }

    public Clothing getBoots() {
        return boots;
    }
}
