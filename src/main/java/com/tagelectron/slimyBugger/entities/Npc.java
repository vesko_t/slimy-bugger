package com.tagelectron.slimyBugger.entities;

import com.tagelectron.engine.collision.Collidable;
import com.tagelectron.engine.collision.Collision;
import com.tagelectron.engine.events.EventType;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.animations.TextureAnimation;
import com.tagelectron.engine.graphics.gui.GuiBar;
import com.tagelectron.engine.graphics.gui.GuiManager;
import com.tagelectron.engine.io.Input;
import com.tagelectron.engine.io.Window;
import com.tagelectron.engine.rendering.Camera;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.text.GLText;
import com.tagelectron.engine.text.GLTrueTypeFont;
import com.tagelectron.engine.world.Tile;
import com.tagelectron.slimyBugger.animations.AnimationType;
import com.tagelectron.slimyBugger.entities.dialog.Dialog;
import com.tagelectron.slimyBugger.gui.MovableGuiTextBox;
import com.tagelectron.slimyBugger.items.Item;
import com.tagelectron.slimyBugger.rendering.CustomRenderer;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;

import java.util.List;

/**
 * Created by vesko on 10/30/2018.
 */
public class Npc extends Entity {

    private MovableGuiTextBox textBox;

    private MovableGuiTextBox dialogBox;

    private GLTrueTypeFont trueTypeFont;

    private String name;

    private List<Dialog> dialogList;

    private String trigger;

    private static Texture dialogTexture = new Texture("/images/textfield.png");

    public Npc(Vector2f position, Shader shader, Vector2f scale, GuiBar bar, MovableGuiTextBox textBox, String name, List<Dialog> dialogs) {
        super(position, shader, scale, "CollisionLayer", new Vector2i(2), 200, bar, 500, 6);
        this.textBox = textBox;
        textBox.setCollidable(this);
        entityBox.setScale(new Vector2f(22, 22));
        loadAnimations();
        animation = animations.get(AnimationType.IdleDown);
        trueTypeFont = new GLTrueTypeFont("/fonts/arial.ttf", 30);
        dialogList = dialogs;
        GLText dialogText = new GLText(trueTypeFont, dialogs.get(0).getStatements().get(0).getText(), CustomRenderer.getTextShader(), Camera.getWidth() - 120);
        //dialogText.setColor(dialogs.get(0).getStatements().get(0).getTextColor());
        dialogBox = new MovableGuiTextBox(new Vector2f(10, Camera.getHeight() - 210),
                new Vector2f(Camera.getWidth() / 2 - 10, 100),
                dialogText,
                dialogTexture, false, new Vector2f(70));
        GuiManager.addGuiElement(dialogBox);
        this.name = name;
    }

    @Override
    protected void checkEntityAround(Collidable collidable) {
        if (collidable instanceof Player) {
            float distance = position.distance(collidable.getPosition());
            if (distance < 80) {
                textBox.setText("Hi player");
                if (Input.isKeyPressed(GLFW.GLFW_KEY_E)) {
                    busy = true;
                    CustomWorld.getPlayer().setBusy(true);
                    dialogBox.setVisible(true);
                }
            }
        }
    }

    @Override
    public void onEvent(EventType eventType) {

    }

    @Override
    protected void onCollision(Tile tile, Collision collision) {

    }

    @Override
    protected void onCollision(Collidable collidable, Collision collision) {

    }

    @Override
    public void update(float v) {
        textBox.setText(name);
        updateCollision();
        updateVelocity();
        if (busy && Input.isKeyPressed(GLFW.GLFW_KEY_SPACE)) {
            dialogBox.setVisible(false);
            busy = false;
            CustomWorld.getPlayer().setBusy(false);
        }
    }

    private void loadAnimations() {
        Texture texture = new Texture("/images/sprites/npc.png");

        animations.put(AnimationType.IdleUp, new TextureAnimation(texture, 1, 21, 13, 8, 1, this));
        animations.put(AnimationType.IdleDown, new TextureAnimation(texture, 1, 21, 13, 10, 1, this));
        animations.put(AnimationType.IdleLeft, new TextureAnimation(texture, 1, 21, 13, 9, 1, this));
        animations.put(AnimationType.IdleRight, new TextureAnimation(texture, 1, 21, 13, 11, 1, this));

    }


    @Override
    protected void onAttack(Entity target) {

    }

    @Override
    public void constantUpdate(float v) {

    }
}
