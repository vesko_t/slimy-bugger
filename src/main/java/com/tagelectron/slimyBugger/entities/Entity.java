package com.tagelectron.slimyBugger.entities;

import com.tagelectron.engine.audio.AudioSource;
import com.tagelectron.engine.collision.Collidable;
import com.tagelectron.engine.events.EventHandler;
import com.tagelectron.engine.events.EventManager;
import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.animations.Animation;
import com.tagelectron.engine.graphics.animations.TextureAnimation;
import com.tagelectron.engine.graphics.gui.GuiBar;
import com.tagelectron.engine.graphics.projectiles.Projectile;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.slimyBugger.CustomUpdater;
import com.tagelectron.slimyBugger.animations.AnimationType;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector4f;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vesko on 10/31/2018.
 */
public abstract class Entity extends Collidable implements EventHandler {

    protected final float movementConst;

    protected GuiBar bar;

    protected final int maxHealth;

    protected int health;

    protected Map<AnimationType, Animation> animations = new HashMap<AnimationType, Animation>();

    protected boolean busy;

    protected boolean attacking = false;
    protected Animation attackingAnimation = null;
    protected Projectile projectile;
    protected Shader projectileShader = new Shader("projectile");
    protected Model projectileModel;
    protected int stepsSound;
    protected int swooshSound;
    protected AudioSource audioSource;
    protected AudioSource swooshSource;
    protected int attackFrame;
    protected Entity target;
    protected boolean inCombat = false;
    protected int attackModifier = 1;

    public Entity(Texture texture, Vector2f position, Shader shader, Vector2f scale, String collisionLayerName, Vector2i collisionRadius, float movementConst, GuiBar bar, int maxHealth, int attackFrame) {
        super(texture, position, shader, scale, collisionLayerName, collisionRadius);
        this.movementConst = movementConst;
        this.bar = bar;
        this.maxHealth = maxHealth;
        this.health = maxHealth;
        this.attackFrame = attackFrame;
        EventManager.getEventHandlers().add(this);
    }

    public Entity(Vector2f position, Shader shader, Vector2f scale, String collisionLayerName, Vector2i collisionRadius, float movementConst, GuiBar bar, int maxHealth, int attackFrame) {
        super(position, shader, scale, collisionLayerName, collisionRadius);
        this.movementConst = movementConst;
        this.bar = bar;
        this.maxHealth = maxHealth;
        this.health = maxHealth;
        this.attackFrame = attackFrame;
        EventManager.getEventHandlers().add(this);
    }

    public Entity(Texture texture, Vector2f position, Shader shader, Vector2f scale, String collisionLayerName, Vector2i collisionRadius, Model model, float movementConst, GuiBar bar, int maxHealth) {
        super(texture, position, shader, scale, collisionLayerName, collisionRadius, model);
        this.movementConst = movementConst;
        this.bar = bar;
        this.maxHealth = maxHealth;
        this.health = maxHealth;
        EventManager.getEventHandlers().add(this);
    }

    public Entity(Vector2f position, Shader shader, Vector2f scale, String collisionLayerName, Vector2i collisionRadius, Model model, float movementConst, GuiBar bar, int maxHealth) {
        super(position, shader, scale, collisionLayerName, collisionRadius, model);
        this.movementConst = movementConst;
        this.bar = bar;
        this.maxHealth = maxHealth;
        this.health = maxHealth;
        EventManager.getEventHandlers().add(this);
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
        float healthPercent = ((float) health / (float) maxHealth) * 100.0f;
        bar.setValue(healthPercent);
        bar.setColor(new Vector4f(1 - healthPercent / 100.0f, healthPercent / 100.0f, 0, 1));
    }

    protected void updateAnimation() {
        if (attacking && attackingAnimation != null) {
            if (!animation.equals(attackingAnimation)) {
                animation.stop();
                animation = attackingAnimation;
                animation.start();
                swooshSource.start(swooshSound);
                return;
            } else if (animation.isRunning()) {
                if (animation.getPointer() == attackFrame - 1) {
                    if (target != null) {
                        onAttack(target);
                    }
                }
                return;
            } else {
                attacking = false;
                animation.stop();
                attackingAnimation.stop();
            }

        }

        if (isMovingUp && !isMovingRight && !isMovingLeft) {
            if (!animation.equals(animations.get(AnimationType.WalkUp))) {
                animation.stop();
                animation = animations.get(AnimationType.WalkUp);
                animation.start();
            }
        } else if (isMovingDown && !isMovingRight && !isMovingLeft) {
            if (!animation.equals(animations.get(AnimationType.WalkDown))) {
                animation.stop();
                animation = animations.get(AnimationType.WalkDown);
                animation.start();
            }
        }

        if (isMovingRight) {
            if (!animation.equals(animations.get(AnimationType.WalkRight))) {
                animation.stop();
                animation = animations.get(AnimationType.WalkRight);
                animation.start();
            }
        } else if (isMovingLeft) {
            if (!animation.equals(animations.get(AnimationType.WalkLeft))) {
                animation.stop();
                animation = animations.get(AnimationType.WalkLeft);
                animation.start();
            }
        }

        if (!isMoving || CustomUpdater.isPaused()) {
            audioSource.stop();
            if (isFacingUp) {
                animation.stop();
                animation = animations.get(AnimationType.IdleUp);
                animation.start();
            } else if (isFacingDown) {
                animation.stop();
                animation = animations.get(AnimationType.IdleDown);
                animation.start();
            }

            if (isFacingLeft) {
                animation.stop();
                animation = animations.get(AnimationType.IdleLeft);
                animation.start();
            } else if (isFacingRight) {
                animation.stop();
                animation = animations.get(AnimationType.IdleRight);
                animation.start();
            }
        } else {
            if (!audioSource.isPlaying() && CustomUpdater.getTotalTicks() % 26 == 0) {
                audioSource.start(stepsSound);
            }
        }
    }

    protected void setAttackingAnimation(float angle) {
        attacking = true;

        if (angle >= 45.0f && angle <= 135.0f) {
            attackingAnimation = animations.get(AnimationType.AttackUp);
        } else if (angle <= -45.0f && angle >= -135.0f) {
            attackingAnimation = animations.get(AnimationType.AttackDown);
        }

        if (angle >= -45.0f && angle <= 0 || angle <= 45.0f && angle >= 0) {
            attackingAnimation = animations.get(AnimationType.AttackRight);
        } else if (angle >= 135f && angle <= 180.0f || angle <= -135.0f) {
            attackingAnimation = animations.get(AnimationType.AttackDown);
        }

    }

    public void hit(int damage, boolean isCritical, Entity attacker) {
        setHealth(health - damage);
        if (animations.containsKey(AnimationType.HitColor)) {
            secondaryAnimation = animations.get(AnimationType.HitColor);
            secondaryAnimation.start();
        }
    }

    protected abstract void onAttack(Entity target);

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }

    public float getMovementConst() {
        return movementConst;
    }

    public boolean isInCombat() {
        return inCombat;
    }

    public void setInCombat(boolean inCombat) {
        this.inCombat = inCombat;
    }

    public int getAttackModifier() {
        return attackModifier;
    }

    public void setAttackModifier(int attackModifier) {
        this.attackModifier = attackModifier;
    }
}
