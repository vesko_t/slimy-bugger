package com.tagelectron.slimyBugger.entities.dialog;

import java.util.List;

/**
 * Created by vesko on 19.02.19.
 */
public class Dialog {

    private String trigger;

    private List<Statement> statements;

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }

    public List<Statement> getStatements() {
        return statements;
    }

    public void setStatements(List<Statement> statements) {
        this.statements = statements;
    }
}
