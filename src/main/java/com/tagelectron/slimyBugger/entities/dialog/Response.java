package com.tagelectron.slimyBugger.entities.dialog;

/**
 * Created by vesko on 19.02.19.
 */
public class Response {

    private int id;

    private String text;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
