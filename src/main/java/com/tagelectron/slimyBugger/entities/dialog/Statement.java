package com.tagelectron.slimyBugger.entities.dialog;

import org.joml.Vector3f;

import java.util.List;

/**
 * Created by vesko on 19.02.19.
 */
public class Statement {

    private String text;

    private List<Response> responses;

    private Vector3f textColor;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Response> getResponses() {
        return responses;
    }

    public void setResponses(List<Response> responses) {
        this.responses = responses;
    }

    public Vector3f getTextColor() {
        return textColor;
    }

    public void setTextColor(Vector3f textColor) {
        this.textColor = textColor;
    }
}
