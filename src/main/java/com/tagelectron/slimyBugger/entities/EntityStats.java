package com.tagelectron.slimyBugger.entities;

import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.slimyBugger.entities.dialog.Dialog;

import java.util.List;

/**
 * Created by Vesko on 21.11.2018 г..
 */
public class EntityStats {
    private int health;
    private Texture sprite;
    private int attackPower;
    private float movementSpeed;
    private List<Dialog> dialogs;

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public Texture getSprite() {
        return sprite;
    }

    public void setSprite(Texture sprite) {
        this.sprite = sprite;
    }

    public int getAttackPower() {
        return attackPower;
    }

    public void setAttackPower(int attackPower) {
        this.attackPower = attackPower;
    }

    public float getMovementSpeed() {
        return movementSpeed;
    }

    public void setMovementSpeed(float movementSpeed) {
        this.movementSpeed = movementSpeed;
    }

    public List<Dialog> getDialogs() {
        return dialogs;
    }

    public void setDialogs(List<Dialog> dialogs) {
        this.dialogs = dialogs;
    }
}
