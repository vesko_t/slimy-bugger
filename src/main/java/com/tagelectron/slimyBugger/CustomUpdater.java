package com.tagelectron.slimyBugger;

import com.tagelectron.engine.entities.Updater;
import com.tagelectron.engine.events.EventManager;
import com.tagelectron.engine.events.EventType;
import com.tagelectron.engine.io.Input;
import com.tagelectron.engine.rendering.Camera;
import com.tagelectron.slimyBugger.entities.CustomWorld;
import com.tagelectron.slimyBugger.entities.Player;
import org.joml.Vector2f;
import org.lwjgl.glfw.GLFW;

/**
 * Created by Vesko on 1.11.2018 г..
 */
public class CustomUpdater extends Updater {

    private Vector2f cameraVelocity = new Vector2f();

    public CustomUpdater() {
        super(60);
        paused = true;
    }

    @Override
    public void update(float delta) {
        if (Input.isKeyPressed(GLFW.GLFW_KEY_ESCAPE)) {
            EventManager.sendEvent(EventType.EscapePressed);
        } else if (Input.isKeyPressed(GLFW.GLFW_KEY_F5)) {
            EventManager.sendEvent(EventType.Save);
        } else if (Input.isKeyPressed(GLFW.GLFW_KEY_F9)) {
            Saver.updateSaver();
            EventManager.sendEvent(EventType.LoadLastSave);
        }
    }

    @Override
    public void onEvent(EventType eventType) {
        if (eventType.equals(EventType.WorldLoading)) {
            paused = true;
        } else if (eventType.equals(EventType.WorldLoaded)) {
            paused = false;
        }
    }
}
