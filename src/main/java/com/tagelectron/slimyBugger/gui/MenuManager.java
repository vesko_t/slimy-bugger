package com.tagelectron.slimyBugger.gui;

import com.tagelectron.engine.audio.AudioManager;
import com.tagelectron.engine.entities.Updater;
import com.tagelectron.engine.events.EventHandler;
import com.tagelectron.engine.events.EventManager;
import com.tagelectron.engine.events.EventType;
import com.tagelectron.engine.graphics.Background;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.gui.GuiButton;
import com.tagelectron.engine.graphics.gui.GuiListener;
import com.tagelectron.engine.graphics.gui.GuiManager;
import com.tagelectron.engine.graphics.gui.GuiMenu;
import com.tagelectron.engine.io.Window;
import com.tagelectron.engine.rendering.Camera;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.text.GLText;
import com.tagelectron.engine.text.GLTrueTypeFont;
import com.tagelectron.slimyBugger.CustomUpdater;
import com.tagelectron.slimyBugger.Saver;
import com.tagelectron.slimyBugger.entities.CustomWorld;
import com.tagelectron.slimyBugger.items.ActionBar;
import com.tagelectron.slimyBugger.items.Inventory;
import com.tagelectron.slimyBugger.items.Item;
import com.tagelectron.slimyBugger.items.ItemType;
import com.tagelectron.slimyBugger.items.drops.LootGui;
import com.tagelectron.slimyBugger.rendering.CustomRenderer;
import org.joml.Vector2f;

import java.util.Random;

/**
 * Created by Vesko on 22.11.2018 г..
 */
public class MenuManager implements EventHandler {

    private static final GuiMenu mapMenu = new GuiMenu(true, new Background(new Texture("/images/background.png"), new Shader("background")));
    private static final GuiMenu pauseMenu = new GuiMenu(false, new Background(new Texture("/images/background.png"), new Shader("background")));
    private static final Inventory inventory = new Inventory(new Texture("/images/gui-box.png"), 9);
    private static final LootGui lootGui = new LootGui(new Vector2f(200));
    private final ActionBar actionBar;

    private static final int clickSound = AudioManager.loadSound("/sounds/click1.ogg");
    private static final int releaseSound = AudioManager.loadSound("/sounds/click2.ogg");

    private static final GLTrueTypeFont font = new GLTrueTypeFont("/fonts/arial.ttf", 30, 1024, 1024);

    private static final Texture greenButtonDefaultTexture = new Texture("/ui/green_button00.png");
    private static final Texture greenButtonHoverTexture = new Texture("/ui/green_button02.png");
    private static final Texture greenButtonPressedTexture = new Texture("/ui/green_button03.png");
    private CustomWorld customWorld;

    public MenuManager(CustomWorld customWorld) {
        this.customWorld = customWorld;
        CustomWorld.setInventory(inventory);
        EventManager.getEventHandlers().add(this);

        //pause menu
        GuiButton exitButton = new GuiButton(new Vector2f(Camera.getWidth() / 2.0f - 100.0f, Camera.getHeight() / 2.0f + 50), new Vector2f(100, 25), false, new GLText(font, "Exit", CustomRenderer.getTextShader()), greenButtonDefaultTexture);
        exitButton.setHoverTexture(greenButtonHoverTexture);
        exitButton.setPressedTexture(greenButtonPressedTexture);
        exitButton.setClickSound(clickSound);
        exitButton.setReleaseSound(releaseSound);
        exitButton.setListener(new GuiListener() {
            @Override
            public void onAction(String command) {
                Window.close();
            }
        });

        GuiButton button = new GuiButton(new Vector2f(Camera.getWidth() / 2.0f - 100.0f, Camera.getHeight() / 2.0f - 150), new Vector2f(100, 25), false, new GLText(font, "Resume", CustomRenderer.getTextShader()), greenButtonDefaultTexture);
        button.setHoverTexture(greenButtonHoverTexture);
        button.setPressedTexture(greenButtonPressedTexture);
        button.setClickSound(clickSound);
        button.setReleaseSound(releaseSound);
        button.setListener(new GuiListener() {
            @Override
            public void onAction(String command) {
                CustomUpdater.setPaused(false);
                pauseMenu.setVisible(false);
            }
        });

        GuiButton returnToMainMenu = new GuiButton(new Vector2f(Camera.getWidth() / 2.0f - 150.0f, Camera.getHeight() / 2.0f - 50), new Vector2f(150, 25), false, new GLText(font, "Exit to main menu", CustomRenderer.getTextShader()), greenButtonDefaultTexture);
        returnToMainMenu.setHoverTexture(greenButtonHoverTexture);
        returnToMainMenu.setPressedTexture(greenButtonPressedTexture);
        returnToMainMenu.setClickSound(clickSound);
        returnToMainMenu.setReleaseSound(releaseSound);
        returnToMainMenu.setListener(new GuiListener() {
            @Override
            public void onAction(String command) {
                CustomUpdater.setPaused(true);
                mapMenu.setVisible(true);
                pauseMenu.setVisible(false);
            }
        });

        pauseMenu.getGuiElements().add(button);
        pauseMenu.getGuiElements().add(returnToMainMenu);
        pauseMenu.getGuiElements().add(exitButton);

        //inventory
        Random random = new Random();
        Texture texture = new Texture("/images/tile2.png");
        for (ItemType itemType : ItemType.values()) {
            for (int i = 0; i < 60; i++) {
                int num = random.nextInt();
                inventory.addItem(new Item(texture, itemType, "Test item " + num, "Description of item" + num));
            }
        }

        //main menu
        final GuiButton savedButton = new GuiButton(new Vector2f(Camera.getWidth() / 2.0f - 100.0f, Camera.getHeight() / 2.0f - 150), new Vector2f(100, 25), true, new GLText(font, "Load save", CustomRenderer.getTextShader()), greenButtonDefaultTexture);
        savedButton.setHoverTexture(greenButtonHoverTexture);
        savedButton.setPressedTexture(greenButtonPressedTexture);
        savedButton.setClickSound(clickSound);
        savedButton.setReleaseSound(releaseSound);
        savedButton.setListener(new GuiListener() {
            @Override
            public void onAction(String s) {
                EventManager.sendEvent(EventType.LoadLastSave);
                mapMenu.setVisible(false);
                Updater.setPaused(false);
            }
        });

        final GuiButton guiButton = new GuiButton(new Vector2f(Camera.getWidth() / 2.0f - 100.0f, Camera.getHeight() / 2.0f - 50), new Vector2f(100, 25), true, new GLText(font, "Load Cave", CustomRenderer.getTextShader()), greenButtonDefaultTexture);
        guiButton.setHoverTexture(greenButtonHoverTexture);
        guiButton.setPressedTexture(greenButtonPressedTexture);
        guiButton.setClickSound(clickSound);
        guiButton.setReleaseSound(releaseSound);
        guiButton.setListener(new GuiListener() {
            @Override
            public void onAction(String s) {
                EventType eventType = EventType.LoadNewMap;
                eventType.setCommand("cave");
                EventManager.sendEvent(eventType);
                mapMenu.setVisible(false);
                Updater.setPaused(false);
            }
        });

        final GuiButton topDownMap = new GuiButton(new Vector2f(Camera.getWidth() / 2.0f - 150.0f, Camera.getHeight() / 2.0f + 50), new Vector2f(150, 25), true, new GLText(font, "Load top down map", CustomRenderer.getTextShader()), greenButtonDefaultTexture);
        topDownMap.setHoverTexture(greenButtonHoverTexture);
        topDownMap.setPressedTexture(greenButtonPressedTexture);
        topDownMap.setClickSound(clickSound);
        topDownMap.setReleaseSound(releaseSound);
        topDownMap.setListener(new GuiListener() {
            @Override
            public void onAction(String s) {
                EventType eventType = EventType.LoadNewMap;
                eventType.setCommand("top-down");
                EventManager.sendEvent(eventType);
                mapMenu.setVisible(false);
                Updater.setPaused(false);
            }
        });

        final GuiButton exitBtn = new GuiButton(new Vector2f(Camera.getWidth() / 2.0f - 100.0f, Camera.getHeight() / 2.0f + 150), new Vector2f(100, 25), true, new GLText(font, "Exit", CustomRenderer.getTextShader()), greenButtonDefaultTexture);
        exitBtn.setHoverTexture(greenButtonHoverTexture);
        exitBtn.setPressedTexture(greenButtonPressedTexture);
        exitBtn.setClickSound(clickSound);
        exitBtn.setReleaseSound(releaseSound);
        exitBtn.setListener(new GuiListener() {
            @Override
            public void onAction(String s) {
                Window.close();
            }
        });

        mapMenu.getGuiElements().add(exitBtn);
        mapMenu.getGuiElements().add(topDownMap);
        mapMenu.getGuiElements().add(guiButton);
        if (Saver.getPlayerMap() != null && Saver.getPlayerPosition() != null) {
            mapMenu.getGuiElements().add(savedButton);
        }

        actionBar = new ActionBar();

        GuiManager.getGuiElements().add(actionBar);
        GuiManager.getGuiElements().add(mapMenu);
        GuiManager.getGuiElements().add(pauseMenu);
        GuiManager.getGuiElements().add(inventory);
    }

    @Override
    public void onEvent(EventType eventType) {
        if (eventType.equals(EventType.EscapePressed)) {
            if (mapMenu.isVisible()) {
                Window.close();
            } else if (!pauseMenu.isVisible() && !inventory.isVisible() && !lootGui.isVisible()) {
                pauseMenu.setVisible(true);
                CustomUpdater.setPaused(true);
                actionBar.setVisible(false);
            } else if (pauseMenu.isVisible() || inventory.isVisible() || lootGui.isVisible()) {
                pauseMenu.setVisible(false);
                inventory.setVisible(false);
                lootGui.setVisible(false);
                actionBar.setVisible(true);
                CustomUpdater.setPaused(false);
            }
        } else if (eventType.equals(EventType.PlayerDeath)) {
            mapMenu.setVisible(true);
            pauseMenu.setVisible(false);
            inventory.setVisible(false);
            actionBar.setVisible(true);
            Updater.setPaused(true);
        } else if (eventType.equals(EventType.OpenContainer)) {
            Updater.setPaused(true);
        }
    }

    public static GuiMenu getMapMenu() {
        return mapMenu;
    }

    public static GuiMenu getPauseMenu() {
        return pauseMenu;
    }

    public static Inventory getInventory() {
        return inventory;
    }

    public static LootGui getLootGui() {
        return lootGui;
    }
}
