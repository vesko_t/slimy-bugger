package com.tagelectron.slimyBugger.gui;

import com.tagelectron.engine.collision.Collidable;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.gui.GuiTextBox;
import com.tagelectron.engine.rendering.Camera;
import com.tagelectron.engine.text.GLText;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

/**
 * Created by vesko on 10/31/2018.
 */
public class MovableGuiTextBox extends GuiTextBox {

    private Collidable collidable;

    public MovableGuiTextBox(Vector2f position, Vector2f scale, GLText text, Texture texture, boolean visible, Vector2f textOffset) {
        super(position, scale, text, texture, visible, textOffset);
    }

    public MovableGuiTextBox(Vector2f position, Vector2f scale, GLText text, Texture texture, boolean visible, Vector2f textOffset, Collidable collidable) {
        super(position, scale, text, texture, visible, textOffset);
        this.collidable = collidable;
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        if (collidable != null) {
            setPosition(Camera.getPositionScreen(new Vector2f(collidable.getPosition().x - collidable.getScale().x, collidable.getPosition().y + collidable.getScale().y + 5 + scale.y)));
            scale.x = text.getLength() / 2.0f;
            visible = !collidable.isToBeRemoved();
            modelMatrix = new Matrix4f().identity();
            modelMatrix.scale(new Vector3f(scale, 0));
            modelMatrix.setTranslation(new Vector3f(position, 0));
        }
    }

    @Override
    public void setText(String text) {
        super.setText(text);
        scale.x = (this.text.getLength() + 4) / 2.0f;
        modelMatrix = new Matrix4f().identity();
        modelMatrix.scale(new Vector3f(scale, 0));
    }

    public void setCollidable(Collidable collidable) {
        this.collidable = collidable;
    }
}
