package com.tagelectron.slimyBugger.gui;

import com.tagelectron.engine.collision.Collidable;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.gui.ComplexGui;
import com.tagelectron.engine.graphics.gui.GuiLabel;
import com.tagelectron.engine.rendering.Camera;
import com.tagelectron.engine.text.GLText;
import org.joml.Vector2f;

/**
 * Created by vesko on 10/30/2018.
 */
public class MovableComplexGui extends ComplexGui {

    private Collidable collidable;

    private GuiLabel label;

    public MovableComplexGui(Vector2f position, Vector2f scale, Texture texture, boolean visible, Collidable collidable) {
        super(position, scale, texture, visible);
        this.collidable = collidable;
    }

    public MovableComplexGui(Vector2f position, Vector2f scale, GLText text, Texture texture, boolean visible, Collidable collidable) {
        super(position, scale, texture, visible);
        label = new GuiLabel(position, visible, text);
        this.collidable = collidable;
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        setPosition(Camera.getPositionScreen(new Vector2f(collidable.getPosition().x - collidable.getScale().x, collidable.getPosition().y + collidable.getScale().y + 5 + scale.y)));
        visible = !collidable.isToBeRemoved();
    }
}
