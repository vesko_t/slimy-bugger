package com.tagelectron.slimyBugger.gui;

import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.gui.GuiButton;
import com.tagelectron.engine.text.GLText;
import com.tagelectron.slimyBugger.CustomUpdater;
import org.joml.Vector2f;

/**
 * Created by Vesko on 1.11.2018 г..
 */
public class CustomGuiButton extends GuiButton {

    public CustomGuiButton(Vector2f position, Vector2f scale, boolean visible, GLText text, Texture texture) {
        super(position, scale, visible, text, texture);
    }

    @Override
    public void constantUpdate(float delta) {
        super.constantUpdate(delta);
        visible = CustomUpdater.isPaused();
    }
}
