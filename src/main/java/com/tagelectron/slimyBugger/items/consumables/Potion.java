package com.tagelectron.slimyBugger.items.consumables;

import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.slimyBugger.items.ItemType;

public class Potion extends Consumable {

    public Potion(Texture texture, ItemType type, String name, String description) {
        super(texture, type, name, description);
    }

}
