package com.tagelectron.slimyBugger.items.consumables;

import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.slimyBugger.items.Item;
import com.tagelectron.slimyBugger.items.ItemType;

public class Consumable extends Item {

    public Consumable(Texture texture, ItemType type, String name, String description) {
        super(texture, type, name, description);
    }

}
