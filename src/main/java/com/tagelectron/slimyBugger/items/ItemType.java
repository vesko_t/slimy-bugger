package com.tagelectron.slimyBugger.items;

/**
 * Created by Vesko on 2.11.2018 г..
 */
public enum ItemType {
    Weapons, Clothes, Food, Potions, Junk, Quest
}
