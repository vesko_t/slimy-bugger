package com.tagelectron.slimyBugger.items;

import com.tagelectron.engine.collision.BoundingBox;
import com.tagelectron.engine.entities.Updater;
import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.gui.GuiButton;
import com.tagelectron.engine.graphics.gui.GuiListener;
import com.tagelectron.engine.graphics.gui.TexturedGuiElement;
import com.tagelectron.engine.io.Input;
import com.tagelectron.engine.io.Window;
import com.tagelectron.engine.rendering.Camera;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.text.GLText;
import com.tagelectron.engine.text.GLTrueTypeFont;
import com.tagelectron.slimyBugger.gui.MenuManager;
import com.tagelectron.slimyBugger.items.equipment.EquipmentGui;
import com.tagelectron.slimyBugger.items.utils.DescriptionBox;
import com.tagelectron.slimyBugger.items.utils.ItemUtils;
import com.tagelectron.slimyBugger.rendering.CustomRenderer;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWScrollCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.lwjgl.opengl.GL11.*;

/**
 * Created by vesko on 10/31/2018.
 */
public class Inventory extends TexturedGuiElement implements GuiListener {

    private Map<ItemType, List<List<Item>>> items = new HashMap<>();
    private final int itemsPerRow;
    private ItemType selectedItemType = ItemType.Weapons;
    private static final Map<ItemType, GuiButton> typeButtons = new HashMap<>();
    private Texture buttonHover = new Texture("/ui/green_button02.png");
    private Texture pressedTexture = new Texture("/ui/green_button03.png");
    private Texture defaultTexture = new Texture("/ui/green_button00.png");

    static Model dropModel = new Model();
    private int scrollHeight = 0;
    private int scrollSpeed = 10;
    private EquipmentGui equipmentGui;
    private BoundingBox boundingBox;
    private Vector2i scissorPossition;
    private Vector2i scissorScale;

    public Inventory(Texture texture, int itemsPerRow) {
        super(new Vector2f(Camera.getWidth() / 2.0f - 528, Camera.getHeight() / 2.0f - 300), new Vector2f(528, 300), texture, false);
        this.itemsPerRow = itemsPerRow;
        scissorPossition = new Vector2i((int) (getPosition().x + ItemUtils.itemGuiPadding.x),
                (int) (getPosition().y + ItemUtils.itemGuiPadding.y));

        scissorScale = new Vector2i((int) (scale.x * 2 - ItemUtils.itemGuiPadding.x),
                (int) (scale.y * 2 - ItemUtils.itemGuiPadding.y * 2.0));
        boundingBox = new BoundingBox(position, new Vector2f(scissorScale.x / 2.0f, scissorScale.y / 2.0f));

        int i = 0;
        Vector2f buttonScale = new Vector2f(80, 25);
        GLTrueTypeFont font = new GLTrueTypeFont("/fonts/arial.ttf", 30, 1024, 1024);
        for (ItemType itemType : ItemType.values()) {
            List<List<Item>> itemList = new ArrayList<>();
            itemList.add(new ArrayList<Item>());
            items.put(itemType, itemList);
            GuiButton button = new GuiButton(new Vector2f(getPosition().x + ((((buttonScale.x * 2) + 10) * i)), getPosition().y - buttonScale.y), buttonScale, true, new GLText(font, itemType.toString(), CustomRenderer.getTextShader()), defaultTexture);
            button.setCommand(itemType.toString());
            button.setHoverTexture(buttonHover);
            button.setPressedTexture(pressedTexture);
            button.setListener(this);
            typeButtons.put(itemType, button);
            i++;
        }
        onAction(selectedItemType.toString());
        Window.setScrollCallback(new GLFWScrollCallback() {
            @Override
            public void invoke(long l, double v, double v1) {
                float pageHeight = (float) ((ItemUtils.itemScale.y * 2) * (items.get(selectedItemType).size() - 4f) + ItemUtils.GAP * (items.get(selectedItemType).size() - 3.5) - v1 * scrollSpeed);
                System.out.println(pageHeight);
                if ((scrollHeight < 0 || v1 < 0) && ((-scrollHeight < pageHeight - ItemUtils.itemScale.y * 2 + ItemUtils.GAP) || v1 > 0)) {
                    items.get(selectedItemType).forEach(items1 -> {
                        items1.forEach(item -> {
                            item.getIcon().setPosition(item.getIcon().getPosition().add(new Vector2f(0, (float) v1 * scrollSpeed)));
                        });
                    });
                    scrollHeight += v1 * scrollSpeed;
                }
            }
        });
        equipmentGui = new EquipmentGui(new Vector2f(position.x + scale.x / 1.85f, position.y), new Vector2f(200), ItemUtils.backgroundBox, true);
    }

    @Override
    public void setPosition(Vector2f position) {
        this.position = position;
        modelMatrix.setTranslation(new Vector3f(position, 0));
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        for (ItemType itemType : ItemType.values()) {
            if (typeButtons.get(itemType).isVisible()) {
                typeButtons.get(itemType).update(delta);
            }
        }
        AtomicBoolean isMouseOverItem = new AtomicBoolean(false);
        items.get(selectedItemType).forEach(items1 -> items1.forEach(item -> {
            item.getIcon().update(delta);
            if (item.getIcon().isMouseOver() && boundingBox.isInBoundingBox(Input.getMousePositionScreen())) {
                ItemUtils.selectOutline.setPosition(new Vector2f(item.getIcon().getPosition()));
                ItemUtils.selectOutline.setVisible(true);
                ItemUtils.deceptionBox.setItem(item);
                ItemUtils.deceptionBox.setPosition(Input.getMousePositionScreen());
                ItemUtils.deceptionBox.setVisible(true);
                isMouseOverItem.set(true);
            }
        }));

        if (this.selectedItemType.equals(ItemType.Weapons) || this.selectedItemType.equals(ItemType.Clothes)) {
            equipmentGui.update(delta);
            if (equipmentGui.getBootsIcon().getIcon().isMouseOver()) {
                ItemUtils.selectOutline.setPosition(new Vector2f(equipmentGui.getBootsIcon().getIcon().getPosition()));
                ItemUtils.selectOutline.setVisible(true);
                ItemUtils.deceptionBox.setPosition(Input.getMousePositionScreen());
                ItemUtils.deceptionBox.setItem(equipmentGui.getBootsIcon());
                ItemUtils.deceptionBox.setVisible(true);
                isMouseOverItem.set(true);
            } else if (equipmentGui.getChestIcon().getIcon().isMouseOver()) {
                ItemUtils.selectOutline.setPosition(new Vector2f(equipmentGui.getChestIcon().getIcon().getPosition()));
                ItemUtils.selectOutline.setVisible(true);
                ItemUtils.deceptionBox.setPosition(Input.getMousePositionScreen());
                ItemUtils.deceptionBox.setItem(equipmentGui.getChestIcon());
                ItemUtils.deceptionBox.setVisible(true);
                isMouseOverItem.set(true);
            } else if (equipmentGui.getLegsIcon().getIcon().isMouseOver()) {
                ItemUtils.selectOutline.setPosition(new Vector2f(equipmentGui.getLegsIcon().getIcon().getPosition()));
                ItemUtils.selectOutline.setVisible(true);
                ItemUtils.deceptionBox.setPosition(Input.getMousePositionScreen());
                ItemUtils.deceptionBox.setItem(equipmentGui.getLegsIcon());
                ItemUtils.deceptionBox.setVisible(true);
                isMouseOverItem.set(true);
            }/*else if (equipmentGui.getWeaponIcon().getIcon().isMouseOver()) {
                ItemUtils.selectOutline.setPosition(new Vector2f(equipmentGui.getWeaponIcon().getIcon().getPosition()));
                ItemUtils.selectOutline.setVisible(true);
                ItemUtils.deceptionBox.setPosition(Input.getMousePositionScreen());
                ItemUtils.deceptionBox.setItem(equipmentGui.getBootsIcon());
                ItemUtils.deceptionBox.setVisible(true);
                isMouseOverItem.set(true);
            }*/
        }
        if (!isMouseOverItem.get()) {
            ItemUtils.selectOutline.setVisible(false);
            ItemUtils.deceptionBox.setVisible(false);
        }
    }

    @Override
    public void render(Shader textureShader, Shader noTextureShader, Model model) {
        super.render(textureShader, noTextureShader, model);
        if (this.selectedItemType.equals(ItemType.Weapons) || this.selectedItemType.equals(ItemType.Clothes)) {
            equipmentGui.render(textureShader, noTextureShader, model);
        }
        for (ItemType itemType : ItemType.values()) {
            typeButtons.get(itemType).render(textureShader, noTextureShader, model);
        }
        glEnable(GL_SCISSOR_TEST);
        glScissor(scissorPossition.x, scissorPossition.y, scissorScale.x, scissorScale.y);
        for (List<Item> row : items.get(selectedItemType)) {
            for (Item item : row) {
                item.getIcon().render(textureShader, noTextureShader, model);
            }
        }
        if (ItemUtils.selectOutline.isVisible()) {
            ItemUtils.selectOutline.render(textureShader, noTextureShader, model);
        }
        glDisable(GL_SCISSOR_TEST);
        if (ItemUtils.deceptionBox.isVisible()) {
            ItemUtils.deceptionBox.render(textureShader, noTextureShader, model);
        }

    }

    @Override
    public void constantUpdate(float delta) {
        super.constantUpdate(delta);
        if (Input.isKeyPressed(GLFW.GLFW_KEY_I)) {
            if (!MenuManager.getMapMenu().isVisible() && !MenuManager.getPauseMenu().isVisible()) {
                visible = true;
                Updater.setPaused(true);
            } else if (visible) {
                visible = false;
                Updater.setPaused(false);
            }
        }
    }

    public void addItem(Item item) {
        List<List<Item>> items = this.items.get(item.getType());
        int itemsPerRow = this.itemsPerRow;
        if (item.getType().equals(ItemType.Weapons) || item.getType().equals(ItemType.Clothes)) {
            itemsPerRow = 5;
        }
        List<Item> row = items.get(items.size() - 1);
        Vector2f itemPosition;
        if (row.size() == 0) {
            float gap = (ItemUtils.GAP * (items.size() - 1));
            itemPosition = new Vector2f(getPosition().x + ItemUtils.itemGuiPadding.x, getPosition().y + ItemUtils.itemGuiPadding.y + ((ItemUtils.itemScale.y * 2) * (items.size() - 1)) + gap);
        } else {
            Item previousItem = row.get(row.size() - 1);
            itemPosition = new Vector2f(previousItem.getIcon().getPosition().x + (ItemUtils.itemScale.x * 2) + ItemUtils.GAP, previousItem.getIcon().getPosition().y);
        }
        item.getIcon().setPosition(itemPosition);
        row.add(item);
        if (row.size() == itemsPerRow) {
            items.add(new ArrayList<>());
        }
    }

    @Override
    public void onAction(String s) {
        this.goToTop();
        typeButtons.get(selectedItemType).setVisible(true);
        typeButtons.get(selectedItemType).setTexture(defaultTexture);
        selectedItemType = ItemType.valueOf(s);
        typeButtons.get(ItemType.valueOf(s)).setTexture(pressedTexture);
        typeButtons.get(ItemType.valueOf(s)).setVisible(false);
        ItemUtils.selectOutline.setVisible(false);
        ItemUtils.deceptionBox.setVisible(false);

    }

    public void goToTop() {
        items.get(selectedItemType).forEach(items1 -> {
            items1.forEach(item -> {
                item.getIcon().setPosition(item.getIcon().getPosition().add(new Vector2f(0, -scrollHeight)));
            });
        });
        scrollHeight = 0;
    }

    public Map<ItemType, List<List<Item>>> getItems() {
        return items;
    }


}
