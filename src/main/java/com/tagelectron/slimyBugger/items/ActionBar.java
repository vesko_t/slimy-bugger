package com.tagelectron.slimyBugger.items;

import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.gui.GuiElement;
import com.tagelectron.engine.graphics.gui.TexturedGuiElement;
import com.tagelectron.engine.rendering.Shader;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.List;

public class ActionBar extends TexturedGuiElement {
    private List<Item> items = new ArrayList<>(10);

    public ActionBar() {
        super(new Vector2f(240, 670), new Vector2f(400, 50), new Texture("/images/tile2.png"), true);
    }

    @Override
    public void render(Shader shader, Shader shader1, Model model) {
        super.render(shader, shader1, model);
        items.forEach((item -> {
            item.getIcon().render(shader, shader1, model);
        }));
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
