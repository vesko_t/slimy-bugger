package com.tagelectron.slimyBugger.items;

import com.tagelectron.engine.entities.Updater;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.slimyBugger.items.drops.Drop;
import com.tagelectron.slimyBugger.items.utils.ItemIcon;
import com.tagelectron.slimyBugger.items.utils.ItemUtils;
import com.tagelectron.slimyBugger.rendering.CustomRenderer;
import org.joml.Vector2f;

/**
 * Created by vesko on 10/31/2018.
 */
public class Item {

    private ItemType type;
    private final String description;
    private final String name;
    private boolean moving;

    private ItemIcon icon;

    private Drop drop;

    public Item(Texture texture, ItemType type, String name, String description) {
        icon = new ItemIcon(texture, true, this);
        this.type = type;
        this.name = name;
        this.description = description;
        drop = new Drop(texture, new Vector2f(), ItemUtils.dropShader, new Vector2f(16), this, Inventory.dropModel);
    }


    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }

    public boolean isMoving() {
        return moving;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void drop(Vector2f position) {
        drop.setPosition(position);
        CustomRenderer.getRenderables().add(drop);
        Updater.getUpdateables().add(drop);
    }

    public Drop getDrop() {
        return drop;
    }

    public void setDrop(Drop drop) {
        this.drop = drop;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public ItemIcon getIcon() {
        return icon;
    }

    public void setIcon(ItemIcon icon) {
        this.icon = icon;
    }
}
