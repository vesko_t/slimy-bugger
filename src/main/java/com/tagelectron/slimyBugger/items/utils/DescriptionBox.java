package com.tagelectron.slimyBugger.items.utils;

import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.gui.ComplexGui;
import com.tagelectron.engine.graphics.gui.GuiElement;
import com.tagelectron.engine.graphics.gui.TexturedGuiElement;
import com.tagelectron.engine.rendering.Renderer;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.text.GLText;
import com.tagelectron.engine.text.GLTrueTypeFont;
import com.tagelectron.slimyBugger.items.Item;
import com.tagelectron.slimyBugger.items.ItemType;
import com.tagelectron.slimyBugger.items.equipment.Weapon;
import com.tagelectron.slimyBugger.rendering.CustomRenderer;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class DescriptionBox extends TexturedGuiElement {

    private Item item;

    private GLTrueTypeFont font = new GLTrueTypeFont("/fonts/arial.ttf", 15, 1024, 1024);
    private GLTrueTypeFont bigFont = new GLTrueTypeFont("/fonts/arial.ttf", 20, 1024, 1024);

    private GLText name;
    private GLText description;
    private GLText attack;
    private GLText deffence;
    private static final Vector3f textColor = new Vector3f(1);

    public DescriptionBox(Vector2f position, Vector2f scale, Texture texture, boolean visible) {
        super(position, scale, texture, visible);
        name = new GLText(bigFont, new Vector2f(), CustomRenderer.getTextShader(), textColor);
        description = new GLText(font, new Vector2f(), CustomRenderer.getTextShader(), textColor);
        attack = new GLText(font, new Vector2f(), CustomRenderer.getTextShader(), textColor);
        deffence = new GLText(font, new Vector2f(), CustomRenderer.getTextShader(), textColor);


    }

    public void setItem(Item item) {
        this.item = item;
        name.setText(item.getName());
        description.setText(item.getDescription());
        if (item instanceof Weapon) {
            Weapon weapon = (Weapon) item;
            attack.setText(weapon.getDamageRange().x + "-" + weapon.getDamageRange().y);
            attack.setPosition(new Vector2f(position.x - scale.x + 3, position.y - scale.y + 5 + 15));
        }
    }

    @Override
    public void render(Shader textureShader, Shader noTextureShader, Model model) {
        super.render(textureShader, noTextureShader, model);
        name.setPosition(new Vector2f(position.x - scale.x + 5, position.y - scale.y + 5));
        description.setPosition(new Vector2f(position.x - scale.x + 5, position.y - scale.y + 80));
        attack.setPosition(new Vector2f(position.x - scale.x + 3, position.y - scale.y + 5 + 15));
        name.render();
        description.render();
        if (item.getType().equals(ItemType.Weapons)) {
            attack.render();
        }
    }
}
