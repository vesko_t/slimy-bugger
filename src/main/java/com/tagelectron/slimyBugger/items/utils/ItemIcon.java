package com.tagelectron.slimyBugger.items.utils;

import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.gui.TexturedGuiElement;
import com.tagelectron.engine.io.Input;
import com.tagelectron.slimyBugger.items.Inventory;
import com.tagelectron.slimyBugger.items.Item;
import org.joml.Vector2f;

public class ItemIcon extends TexturedGuiElement {

    private Item item;

    public ItemIcon(Texture texture, boolean visible, Item item) {
        super(new Vector2f(), ItemUtils.itemScale, texture, visible);
        this.item = item;
    }
}
