package com.tagelectron.slimyBugger.items.utils;

import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.gui.TexturedGuiElement;
import com.tagelectron.engine.rendering.Shader;
import org.joml.Vector2f;

public final class ItemUtils {

    public static final Vector2f itemScale = new Vector2f(52);

    public static final Shader dropShader = new Shader("drop");

    public static final Texture backgroundBox = new Texture("/images/gui-box.png");

    public static TexturedGuiElement selectOutline = new TexturedGuiElement(new Vector2f(),
            ItemUtils.itemScale, new Texture("/ui/select-outline.png"), false);

    public static DescriptionBox deceptionBox = new DescriptionBox(new Vector2f(),
            new Vector2f(120, 50),
            ItemUtils.backgroundBox, false);

    public static final Vector2f itemGuiPadding = new Vector2f(20, 35);

    public static final float GAP = 10;

    private ItemUtils() {
    }
}
