package com.tagelectron.slimyBugger.items.utils;

import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.slimyBugger.items.Item;
import com.tagelectron.slimyBugger.items.ItemType;
import com.tagelectron.slimyBugger.items.equipment.Clothing;
import com.tagelectron.slimyBugger.items.equipment.Weapon;
import com.tagelectron.slimyBugger.items.equipment.WeaponType;
import org.joml.Vector2i;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.*;

public final class ItemManager {

    private static Map<ItemType, Map<String, Item>> items;

    private ItemManager() {

    }

    public static void init() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        Document document = null;
        try {
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            document = documentBuilder.parse(ItemManager.class.getResourceAsStream("/entity-config/items.xml"));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        if (document == null) {
            System.err.println("Can't load item data");
            System.exit(1);
        }

        Element itemsElement = (Element) document.getElementsByTagName("items").item(0);

        items = new HashMap<>();

        loadWeapons((Element) itemsElement.getElementsByTagName(ItemType.Weapons.toString()).item(0));
        loadClothes((Element) itemsElement.getElementsByTagName(ItemType.Clothes.toString()).item(0));
        loadJunk((Element) itemsElement.getElementsByTagName(ItemType.Junk.toString()).item(0));
    }

    private static void loadClothes(Element clothesElement) {
        Map<String, Item> itemsFromType = new HashMap<>();
        NodeList clothingList = clothesElement.getElementsByTagName("Clothing");
        for (int i = 0; i < clothingList.getLength(); i++) {
            Element clothingElement = (Element) clothingList.item(i);
            itemsFromType.put(clothingElement.getAttribute("name"), new Clothing(new Texture(
                    "/images/items/" + clothingElement.getAttribute("sheet") + "/icon.png"),
                    ItemType.Clothes,
                    1,
                    clothingElement.getAttribute("name"),
                    clothingElement.getAttribute("description"),
                    new Texture("/images/items/" + clothingElement.getAttribute("sheet") + "/sprite.png"),
                    new Texture("/images/items/" + clothingElement.getAttribute("sheet") + "/preview.png")));
        }
        items.put(ItemType.Clothes, itemsFromType);
    }

    private static void loadWeapons(Element weaponsElement) {
        Map<String, Item> itemsFromType = new HashMap<>();
        NodeList weaponsList = weaponsElement.getElementsByTagName("Weapon");
        for (int i = 0; i < weaponsList.getLength(); i++) {
            Element weaponElement = (Element) weaponsList.item(i);
            itemsFromType.put(weaponElement.getAttribute("name"), new Weapon(new Texture(
                    "/images/items/" + weaponElement.getAttribute("sheet") + "/icon.png"),
                    ItemType.Weapons,
                    weaponElement.getAttribute("name"),
                    weaponElement.getAttribute("description"),
                    new Texture("/images/items/" + weaponElement.getAttribute("sheet") + "/sprite.png"),
                    1, new Vector2i(Integer.parseInt(weaponElement.getAttribute("damageMin")),
                    Integer.parseInt(weaponElement.getAttribute("damageMax"))),
                    Integer.parseInt(weaponElement.getAttribute("criticalChance")),
                    WeaponType.valueOf(weaponElement.getAttribute("type"))));
        }
        items.put(ItemType.Weapons, itemsFromType);
    }

    private static void loadJunk(Element junkElement) {
        Map<String, Item> itemsFromType = new HashMap<>();
        NodeList junkList = junkElement.getElementsByTagName("Junk");
        for (int i = 0; i < junkList.getLength(); i++) {
            Element elementOfJunk = (Element) junkList.item(i);
            itemsFromType.put(elementOfJunk.getAttribute("name"), new Item(new Texture(
                    "/images/" + elementOfJunk.getAttribute("texture") + ".png"),
                    ItemType.Junk,
                    elementOfJunk.getAttribute("name"),
                    elementOfJunk.getAttribute("description")));
        }
        items.put(ItemType.Junk, itemsFromType);
    }

    public String removeLastCharOptional(String s) {
        return Optional.ofNullable(s)
                .filter(str -> str.length() != 0)
                .map(str -> str.substring(0, str.length() - 1))
                .orElse(s);
    }

    public static Map<ItemType, Map<String, Item>> getItems() {
        return items;
    }
}
