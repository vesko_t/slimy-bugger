package com.tagelectron.slimyBugger.items.drops;

import com.tagelectron.engine.collision.BoundingBox;
import com.tagelectron.engine.entities.Updater;
import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.gui.GuiManager;
import com.tagelectron.engine.graphics.gui.TexturedGuiElement;
import com.tagelectron.engine.io.Input;
import com.tagelectron.engine.io.Window;
import com.tagelectron.engine.rendering.Camera;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.text.GLText;
import com.tagelectron.slimyBugger.items.Item;
import com.tagelectron.slimyBugger.items.utils.ItemUtils;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWScrollCallback;
import org.lwjgl.system.CallbackI;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glScissor;

public class LootGui extends TexturedGuiElement {

    private Container container;

    private static final float GAP = 15.0f;
    private static final int itemsPerRow = 3;

    private BoundingBox boundingBox;
    private Vector2i scissorPossition;
    private Vector2i scissorScale;
    private int scrollHeight = 0;
    private int scrollSpeed = 10;

    public LootGui(Vector2f scale) {
        super(new Vector2f(Camera.getWidth() / 2.0f - scale.x, Camera.getHeight() / 2.0f - scale.y), scale, ItemUtils.backgroundBox, false);
        GuiManager.addGuiElement(this);
        scissorPossition = new Vector2i((int) (getPosition().x + ItemUtils.itemGuiPadding.x),
                (int) (getPosition().y + ItemUtils.itemGuiPadding.y));

        scissorScale = new Vector2i((int) (scale.x * 2 - ItemUtils.itemGuiPadding.x),
                (int) (scale.y * 2 - ItemUtils.itemGuiPadding.y * 2.0));
        boundingBox = new BoundingBox(position, new Vector2f(scissorScale.x / 2.0f, scissorScale.y / 2.0f));
        Window.setScrollCallback(new GLFWScrollCallback() {
            @Override
            public void invoke(long l, double v, double v1) {
                float pageHeight = container.getItems().size() * (ItemUtils.itemScale.y * 2 + GAP);
                System.out.println(pageHeight);
                float lastItemPos = container.getItems().get(container.getItems().size() - 1).getIcon().getPosition().y - (ItemUtils.itemScale.y * 2.0f);
                System.out.println(lastItemPos);
                if ((scrollHeight < 0 || v1 < 0) && ((lastItemPos >= boundingBox.getPosition().y - boundingBox.getScale().y) || v1 < 0)) {
                    container.getItems().forEach(item -> {
                        item.getIcon().setPosition(item.getIcon().getPosition().add(new Vector2f(0, (float) v1 * scrollSpeed)));
                    });
                    scrollHeight += v1 * scrollSpeed;
                }
            }
        });
    }

    @Override
    public void constantUpdate(float delta) {
        super.constantUpdate(delta);
        if (Input.isKeyPressed(GLFW.GLFW_KEY_ESCAPE)) {
            Updater.setPaused(false);
        }
    }

    public void openContainer(Container container) {
        this.container = container;
        visible = true;
        Vector2f iconPosition = getPosition().add(ItemUtils.itemGuiPadding);
        int itemCount = 0;
        int rowNum = 0;
        for (int i = 0; i < container.getItems().size(); i++) {
            Item item = container.getItems().get(i);
            item.getIcon().setPosition(iconPosition);
            iconPosition.add(
                    new Vector2f((ItemUtils.itemScale.x * 2 + GAP), 0));
            itemCount++;
            if (itemCount == itemsPerRow) {
                rowNum++;
                iconPosition.x = getPosition().add(ItemUtils.itemGuiPadding).x;
                iconPosition.y += ItemUtils.itemScale.y * 2 + GAP;
                itemCount = 0;
            }
        }

    }

    @Override
    public void render(Shader textureShader, Shader noTextureShader, Model model) {
        super.render(textureShader, noTextureShader, model);
        container.getItems().forEach(item -> {
            glEnable(GL_SCISSOR_TEST);
            glScissor(scissorPossition.x, scissorPossition.y, scissorScale.x, scissorScale.y);
            item.getIcon().render(textureShader, noTextureShader, model);
            if (ItemUtils.selectOutline.isVisible()) {
                ItemUtils.selectOutline.render(textureShader, noTextureShader, model);
            }
            glDisable(GL_SCISSOR_TEST);
            if (ItemUtils.deceptionBox.isVisible()) {
                ItemUtils.deceptionBox.render(textureShader, noTextureShader, model);
            }
        });
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        boolean isMouseOverItem = false;
        for (Item item : container.getItems()) {
            item.getIcon().update(delta);
            if (item.getIcon().isMouseOver() && boundingBox.isInBoundingBox(Input.getMousePositionScreen())) {
                ItemUtils.selectOutline.setPosition(new Vector2f(item.getIcon().getPosition()));
                ItemUtils.selectOutline.setVisible(true);
                ItemUtils.deceptionBox.setItem(item);
                ItemUtils.deceptionBox.setPosition(Input.getMousePositionScreen());
                ItemUtils.deceptionBox.setVisible(true);
                isMouseOverItem = true;
            }
        }
        if (!isMouseOverItem) {
            ItemUtils.selectOutline.setVisible(false);
            ItemUtils.deceptionBox.setVisible(false);
        }
    }
}
