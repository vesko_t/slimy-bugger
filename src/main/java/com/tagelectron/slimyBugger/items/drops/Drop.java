package com.tagelectron.slimyBugger.items.drops;

import com.tagelectron.engine.collision.Collidable;
import com.tagelectron.engine.collision.Collision;
import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.rendering.Renderable;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.world.Tile;
import com.tagelectron.slimyBugger.entities.Player;
import com.tagelectron.slimyBugger.items.Inventory;
import com.tagelectron.slimyBugger.items.Item;
import org.joml.Vector2f;
import org.joml.Vector2i;

public class Drop extends Collidable {

    private Item item;

    public Drop(Texture texture, Vector2f position, Shader shader, Vector2f scale, Item item, Model model) {
        super(texture, position, shader, scale, "CollisionLayer", new Vector2i(2), model);
        this.item = item;
    }

    @Override
    protected void checkEntityAround(Collidable collidable) {
        if (collidable instanceof Player) {
            Vector2f distgance = new Vector2f();
            collidable.getPosition().sub(position, distgance);
            if (distgance.length() < 41) {
                ((Player) collidable).getInventory().addItem(item);
                toBeRemoved = true;
            }
        }
    }

    @Override
    protected void onCollision(Tile tile, Collision collision) {

    }

    @Override
    protected void onCollision(Collidable collidable, Collision collision) {
    }

    @Override
    public void update(float v) {
        updateCollision();
    }

    @Override
    public void constantUpdate(float v) {

    }
}
