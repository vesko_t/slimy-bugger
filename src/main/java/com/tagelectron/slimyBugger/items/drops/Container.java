package com.tagelectron.slimyBugger.items.drops;

import com.tagelectron.engine.collision.Collidable;
import com.tagelectron.engine.collision.Collision;
import com.tagelectron.engine.events.EventManager;
import com.tagelectron.engine.events.EventType;
import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.io.Input;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.world.Tile;
import com.tagelectron.slimyBugger.entities.CustomWorld;
import com.tagelectron.slimyBugger.entities.Player;
import com.tagelectron.slimyBugger.gui.MenuManager;
import com.tagelectron.slimyBugger.items.Item;
import com.tagelectron.slimyBugger.items.utils.ItemUtils;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.lwjgl.glfw.GLFW;

import javax.swing.text.PlainDocument;
import java.util.ArrayList;
import java.util.List;

public class Container extends Collidable {

    private List<Item> items = new ArrayList<>();

    public Container(Texture texture, Vector2f position, Vector2f scale) {
        super(texture, position, ItemUtils.dropShader, scale, "CollisionLayer", new Vector2i(2));
    }

    @Override
    protected void checkEntityAround(Collidable collidable) {
        if (collidable instanceof Player) {
            //System.out.println("Player");
            if (Input.isKeyPressed(GLFW.GLFW_KEY_E)) {
                /*items.forEach(item -> {
                    ((Player) collidable).getInventory().addItem(item);
                });*/
                EventManager.sendEvent(EventType.OpenContainer);
                MenuManager.getLootGui().openContainer(this);
            }
        }
    }

    @Override
    protected void onCollision(Tile tile, Collision collision) {

    }

    @Override
    protected void onCollision(Collidable collidable, Collision collision) {

    }

    @Override
    public void update(float v) {
        updateCollision();
    }

    @Override
    public void constantUpdate(float v) {

    }

    public List<Item> getItems() {
        return items;
    }
}
