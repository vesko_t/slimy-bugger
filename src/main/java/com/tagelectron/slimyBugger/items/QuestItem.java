package com.tagelectron.slimyBugger.items;

import com.tagelectron.engine.graphics.Texture;

public class QuestItem extends Item {

    public QuestItem(Texture texture, ItemType type, String name, String description) {
        super(texture, type, name, description);
    }
}
