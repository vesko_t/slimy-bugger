package com.tagelectron.slimyBugger.items.equipment;

import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.slimyBugger.items.ItemType;

public class Clothing extends Equipable {

    private Texture preview;

    public Clothing(Texture texture, ItemType type, int level, String name, String description, Texture spriteSheet, Texture preview) {
        super(texture, type, level, name, description, spriteSheet);
        this.preview = preview;
    }

    public Texture getPreview() {
        return preview;
    }

}
