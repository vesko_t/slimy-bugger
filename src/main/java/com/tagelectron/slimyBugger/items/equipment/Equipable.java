package com.tagelectron.slimyBugger.items.equipment;

import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.animations.TextureAnimation;
import com.tagelectron.slimyBugger.animations.AnimationType;
import com.tagelectron.slimyBugger.items.Item;
import com.tagelectron.slimyBugger.items.ItemType;

import java.util.HashMap;
import java.util.Map;

public class Equipable extends Item {

    private Texture spriteSheet;


    protected int level;

    public Equipable(Texture texture, ItemType type, int level, String name, String description, Texture spriteSheet) {
        super(texture, type, name, description);
        this.spriteSheet = spriteSheet;
        this.level = level;
    }

    public Texture getSpriteSheet() {
        return spriteSheet;
    }

    public void setSpriteSheet(Texture spriteSheet) {
        this.spriteSheet = spriteSheet;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

}
