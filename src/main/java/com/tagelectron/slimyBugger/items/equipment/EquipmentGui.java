package com.tagelectron.slimyBugger.items.equipment;

import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.gui.GuiManager;
import com.tagelectron.engine.graphics.gui.TexturedGuiElement;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.slimyBugger.entities.CustomWorld;
import com.tagelectron.slimyBugger.entities.Player;
import com.tagelectron.slimyBugger.items.Item;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class EquipmentGui extends TexturedGuiElement {

    private Texture basePreview;
    private Matrix4f previewMatrix;
    private Item weaponIcon;
    private Item chestIcon;
    private Item legsIcon;
    private Item bootsIcon;
    private Player player;

    public EquipmentGui(Vector2f position, Vector2f scale, Texture texture, boolean visible) {
        super(position.sub(scale), scale, texture, visible);
        this.basePreview = new Texture("/images/player-preview.png");
        previewMatrix = new Matrix4f();
        previewMatrix.setTranslation(new Vector3f(this.position, 0));
        previewMatrix.scale(new Vector3f(scale.mul(1.5f, new Vector2f()), 0));
    }

    @Override
    public void render(Shader textureShader, Shader noTextureShader, Model model) {
        super.render(textureShader, noTextureShader, model);
        textureShader.bind();
        textureShader.setUniform("sampler", 0);
        textureShader.setUniform("projection", GuiManager.getProjection());
        textureShader.setUniform("model", previewMatrix);
        if (player != null) {
            basePreview.bind(0);
            model.render();
            player.getBoots().getPreview().bind(0);
            model.render();
            player.getLegs().getPreview().bind(0);
            model.render();
            player.getChest().getPreview().bind(0);
            model.render();
            chestIcon.getIcon().render(textureShader, noTextureShader, model);
            legsIcon.getIcon().render(textureShader, noTextureShader, model);
            bootsIcon.getIcon().render(textureShader, noTextureShader, model);
        }
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        player = CustomWorld.getPlayer();
        chestIcon = player.getChest();
        legsIcon = player.getLegs();
        bootsIcon = player.getBoots();
        if (chestIcon != null) {
            chestIcon.getIcon().setPosition(new Vector2f(position.x - chestIcon.getIcon().getScale().x - scale.x / 1.5f, position.y - chestIcon.getIcon().getScale().y + scale.y / 1.5f));
        }
        if (legsIcon != null) {
            legsIcon.getIcon().setPosition(new Vector2f(position.x - legsIcon.getIcon().getScale().x, position.y - legsIcon.getIcon().getScale().y + scale.y / 1.5f));
        }
        if (bootsIcon != null) {
            bootsIcon.getIcon().setPosition(new Vector2f(position.x - bootsIcon.getIcon().getScale().x + scale.x / 1.5f, position.y - bootsIcon.getIcon().getScale().y + scale.y / 1.5f));
        }
    }

    public Item getWeaponIcon() {
        return weaponIcon;
    }

    public Item getChestIcon() {
        return chestIcon;
    }

    public Item getLegsIcon() {
        return legsIcon;
    }

    public Item getBootsIcon() {
        return bootsIcon;
    }
}
