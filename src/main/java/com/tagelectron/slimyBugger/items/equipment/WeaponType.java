package com.tagelectron.slimyBugger.items.equipment;

public enum WeaponType {
    Staff, Sword
}
