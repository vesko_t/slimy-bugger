package com.tagelectron.slimyBugger.items.equipment;

import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.slimyBugger.entities.Entity;
import com.tagelectron.slimyBugger.items.ItemType;
import org.joml.Vector2i;

import java.util.Random;

public class Weapon extends Equipable {

    private Vector2i damageRange;

    private Random random;

    private int criticalChance;

    private WeaponType weaponType;

    private int multiplier = 1;

    public Weapon(Texture texture, ItemType type, String name, String description, Texture spriteSheet, int level, Vector2i damageRange, int criticalChance, WeaponType weaponType) {
        super(texture, type, level, name, description, spriteSheet);
        this.setLevel(level);
        this.damageRange = damageRange;
        this.criticalChance = criticalChance;
        random = new Random();
        this.weaponType = weaponType;
    }

    public void attack(Entity attacker, Entity target) {
        int damage = (random.nextInt((damageRange.y - damageRange.x) + 1) + damageRange.x) * attacker.getAttackModifier();
        int criticalIndex = random.nextInt(100);
        boolean isCritical = false;
        if (criticalIndex > 0 && criticalIndex <= criticalChance) {
            damage *= 2;
            isCritical = true;
        }
        target.hit(damage * multiplier, isCritical, attacker);
    }

    public Vector2i getDamageRange() {
        return damageRange;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    @Override
    public void setLevel(int level) {
        this.multiplier = (int) Math.round(Math.pow(1.142, level));
    }
}
