package com.tagelectron.slimyBugger.animations;

import com.tagelectron.engine.graphics.animations.MultipleTextureAnimation;
import com.tagelectron.engine.graphics.animations.TextureAnimation;
import com.tagelectron.engine.io.Timer;

import java.util.List;

public class NamedMultipleTextureAnimation extends MultipleTextureAnimation {

    private static final int weaponIndex = 4;
    private static final int chestIndex = 1;
    private static final int legsIndex = 2;
    private static final int bootsIndex = 3;

    public NamedMultipleTextureAnimation(TextureAnimation baseAnimation) {
        super(baseAnimation.getFPS(), baseAnimation.getTimesToRun(), baseAnimation.getFramesCount());
        addAnimation(baseAnimation);
    }

    public void setBoots(TextureAnimation bootsAnimation) {
        if (getAnimations().size() > bootsIndex) {
            getAnimations().set(bootsIndex, bootsAnimation);
        } else addAnimation(bootsAnimation);
    }

    public void setLegs(TextureAnimation legsAnimation) {
        if (getAnimations().size() > legsIndex) {
            getAnimations().set(legsIndex, legsAnimation);
        } else addAnimation(legsAnimation);
    }

    public void setChest(TextureAnimation chestAnimation) {
        if (getAnimations().size() > chestIndex) {
            getAnimations().set(chestIndex, chestAnimation);
        } else addAnimation(chestAnimation);
    }


    public void setWeapon(TextureAnimation weaponAnimation) {
        if (getAnimations().size() > weaponIndex) {
            getAnimations().set(weaponIndex, weaponAnimation);
        } else addAnimation(weaponAnimation);
    }

    public TextureAnimation getWeapon() {
        if (getAnimations().size() > weaponIndex) {
            return getAnimations().get(weaponIndex);
        } else return null;
    }

    public TextureAnimation getLegs() {
        if (getAnimations().size() > legsIndex) {
            return getAnimations().get(legsIndex);
        } else return null;
    }

    public TextureAnimation getChest() {
        if (getAnimations().size() > chestIndex) {
            return getAnimations().get(chestIndex);
        } else return null;
    }

    public TextureAnimation getBoots() {
        if (getAnimations().size() > bootsIndex) {
            return getAnimations().get(bootsIndex);
        } else return null;
    }
}
