package com.tagelectron.slimyBugger.animations;

import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.animations.MultipleTextureAnimation;
import com.tagelectron.engine.graphics.animations.TextureAnimation;
import com.tagelectron.engine.rendering.Renderable;
import com.tagelectron.slimyBugger.entities.CustomWorld;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class AnimationManager {

    private static final Map<AnimationType, AnimationData> animations = new HashMap<>();

    private static final Vector2i sheetSize = new Vector2i(1536, 4800);

    private AnimationManager() {
    }

    public static void init() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        Document document = null;
        try {
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            document = documentBuilder.parse(AnimationManager.class.getResourceAsStream("/entity-config/animations.xml"));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        if (document == null) {
            System.err.println("Can't load animation data");
            System.exit(1);
        }

        NodeList animationsList = document.getElementsByTagName("animation");

        for (int i = 0; i < animationsList.getLength(); i++) {
            Element animation = (Element) animationsList.item(i);
            animations.put(AnimationType.valueOf(animation.getAttribute("name")),
                    new AnimationData(TextureAnimation.createFrames(Integer.parseInt(animation.getAttribute("cols")),
                            Integer.parseInt(animation.getAttribute("rows")),
                            Integer.parseInt(animation.getAttribute("totalCols")),
                            Integer.parseInt(animation.getAttribute("row")),
                            sheetSize.x,
                            sheetSize.y),
                            Integer.parseInt(animation.getAttribute("fps")),
                            Integer.parseInt(animation.getAttribute("timesToRun"))));
        }
    }

    public static Map<AnimationType, TextureAnimation> getAnimations(Texture texture, Renderable renderable) {
        Map<AnimationType, TextureAnimation> animationMap = new HashMap<>();
        for (AnimationType animation : animations.keySet()) {
            AnimationData animationData = animations.get(animation);
            TextureAnimation textureAnimation = new TextureAnimation(
                    animationData.getFps(),
                    animationData.getTimesToRun(),
                    animationData.getFrames(), renderable, texture);
            animationMap.put(animation, textureAnimation);
        }
        return animationMap;
    }
}
