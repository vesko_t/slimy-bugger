package com.tagelectron.slimyBugger.animations;

import java.util.List;

public class AnimationData {
    private List<Integer> frames;
    private int fps;
    private int timesToRun;

    public AnimationData(List<Integer> frames, int fps, int timesToRun) {
        this.frames = frames;
        this.fps = fps;
        this.timesToRun = timesToRun;
    }

    public List<Integer> getFrames() {
        return frames;
    }

    public void setFrames(List<Integer> frames) {
        this.frames = frames;
    }

    public int getFps() {
        return fps;
    }

    public void setFps(int fps) {
        this.fps = fps;
    }

    public int getTimesToRun() {
        return timesToRun;
    }

    public void setTimesToRun(int timesToRun) {
        this.timesToRun = timesToRun;
    }
}
