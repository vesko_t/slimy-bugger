package com.tagelectron.slimyBugger;

import com.tagelectron.engine.audio.AudioManager;
import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.gui.GuiManager;
import com.tagelectron.engine.io.Window;
import com.tagelectron.engine.rendering.Camera;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.slimyBugger.animations.AnimationManager;
import com.tagelectron.slimyBugger.entities.CustomWorld;
import com.tagelectron.slimyBugger.entities.EntityStats;
import com.tagelectron.slimyBugger.entities.dialog.Dialog;
import com.tagelectron.slimyBugger.entities.dialog.Response;
import com.tagelectron.slimyBugger.entities.dialog.Statement;
import com.tagelectron.slimyBugger.gui.MenuManager;
import com.tagelectron.slimyBugger.items.utils.ItemManager;
import com.tagelectron.slimyBugger.rendering.CustomRenderer;
import org.joml.Vector3f;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Window window = new Window(1280, 720, "Random Game", false, true);

        Window.setIcon(new Texture("/images/icon.png"));

        new Camera(1280, 720);

        AudioManager audioManager = new AudioManager();

        AnimationManager.init();
        ItemManager.init();

        final CustomWorld world = new CustomWorld(new Shader("tile"), new Vector3f(32f), loadEntityInformation());

        new Saver(new File("./saves/save.sav"));

        new GuiManager(new Shader("gui"), new Shader("no-texture-gui"));

        CustomUpdater collisionUpdater = new CustomUpdater();

        CustomRenderer renderer = new CustomRenderer(window, world, collisionUpdater);

        new MenuManager(world);

        renderer.run();

        audioManager.cleanup();

        collisionUpdater.stop();

        Saver.cleanUp();

        Model.cleanUp();
    }

    private static Map<String, EntityStats> loadEntityInformation() {
        Map<String, EntityStats> entityStats = new HashMap<>();

        InputStream is = CustomWorld.class.getResourceAsStream("/entity-config/npc.xml");
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(is);
            Element npcs = (Element) document.getElementsByTagName("npcs").item(0);

            readForEnemy((Element) npcs.getElementsByTagName("enemies").item(0), entityStats);
            readForFriendlies((Element) npcs.getElementsByTagName("friendlies").item(0), entityStats);
        } catch (Exception e) {
            System.err.println("Can't read entity information");
            e.printStackTrace();
            System.exit(1);
        }
        return entityStats;
    }

    private static void readForEnemy(Element enemies, Map<String, EntityStats> entityStats) {
        for (int i = 0; i < enemies.getElementsByTagName("enemy").getLength(); i++) {
            EntityStats stats = new EntityStats();
            Element typeStat = (Element) enemies.getElementsByTagName("enemy").item(i);
            stats.setHealth(Integer.parseInt(typeStat.getAttribute("health")));
            stats.setAttackPower(Integer.parseInt(typeStat.getAttribute("attackPower")));
            stats.setSprite(new Texture("/images/sprites/" + typeStat.getAttribute("sprite")));
            stats.setMovementSpeed(Float.parseFloat(typeStat.getAttribute("movementSpeed")));
            entityStats.put(typeStat.getAttribute("type"), stats);
        }
    }

    private static void readForFriendlies(Element friendlies, Map<String, EntityStats> entityStats) {
        for (int i = 0; i < friendlies.getElementsByTagName("friendly").getLength(); i++) {
            EntityStats stats = new EntityStats();
            Element typeStat = (Element) friendlies.getElementsByTagName("friendly").item(i);
            stats.setSprite(new Texture("/images/sprites/" + typeStat.getAttribute("sprite")));
            List<Dialog> dialogsList = new ArrayList<>();
            stats.setDialogs(dialogsList);
            Element dialogs = (Element) typeStat.getElementsByTagName("dialogs").item(0);
            for (int j = 0; j < dialogs.getElementsByTagName("dialog").getLength(); j++) {
                Element dialogElement = (Element) dialogs.getElementsByTagName("dialog").item(j);
                Dialog dialog = new Dialog();
                dialog.setTrigger(dialogElement.getAttribute("trigger"));
                dialog.setStatements(new ArrayList<>());
                dialogsList.add(dialog);
                for (int k = 0; k < dialogElement.getElementsByTagName("statement").getLength(); k++) {
                    Element statementElement = (Element) dialogElement.getElementsByTagName("statement").item(k);
                    Statement statement = new Statement();
                    statement.setText(statementElement.getAttribute("value"));
                    //statement.setTextColor(new Vector3f(Float.parseFloat(statementElement.getAttribute("r")), Float.parseFloat(statementElement.getAttribute("g")), Float.parseFloat(statementElement.getAttribute("b"))));
                    statement.setResponses(new ArrayList<>());
                    dialog.getStatements().add(statement);
                    for (int l = 0; l < statementElement.getElementsByTagName("response").getLength(); l++) {
                        Element responseElement = (Element) statementElement.getElementsByTagName("response").item(l);
                        Response response = new Response();
                        response.setText(responseElement.getAttribute("value"));
                        response.setId(Integer.parseInt(responseElement.getAttribute("id")));
                        statement.getResponses().add(response);
                    }
                }
            }
            entityStats.put(typeStat.getAttribute("type"), stats);
        }
    }


}
