package com.tagelectron.slimyBugger.rendering;

import com.tagelectron.engine.events.EventType;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.particles.ParticleEmitter;
import com.tagelectron.engine.io.Window;
import com.tagelectron.engine.rendering.Camera;
import com.tagelectron.engine.rendering.Renderer;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.slimyBugger.CustomUpdater;
import com.tagelectron.slimyBugger.entities.CustomWorld;
import org.joml.Vector2f;

public class CustomRenderer extends Renderer {

    private static final Shader textShader = new Shader("text");
    private static final Shader particleShader = new Shader("particles");
    private static final Shader lightningShader = new Shader("lightning");
    
    private static final ParticleEmitter particleEmitter = new ParticleEmitter(
            new Texture("/images/tile2.png"),
            new Vector2f(),
            particleShader,
            new Vector2f(6),
            new Vector2f(),
            240,
            1,
            new Vector2f(-50, 50),
            new Vector2f(-50, 50)
    );

    int vertexId = 0;
    int cout = 0;
    private CustomUpdater customUpdater;

    public CustomRenderer(Window window, CustomWorld world, CustomUpdater customUpdater) {
        super(window, world, false);
        this.customUpdater = customUpdater;
        showFps = false;
    }

    @Override
    protected void setupBloomShaders() {
        Renderer.blurShader = new Shader("blur");
        Renderer.combineShader = new Shader("combine");
    }

    @Override
    public void befreRender() {

    }

    @Override
    public void beforeRenderLights() {

    }

    @Override
    public void afterRenderLights() {

    }

    public void beforeRenderWorld() {
        // t = System.currentTimeMillis();
    }

    public void afterRenderWorldBeforeRenderables() {
       /* if (CustomWorld.getPlayer() != null) {
            particleEmitter.setPosition(CustomWorld.getPlayer().getPosition());
            //particleEmitter.render();
        }*/
    }

    public void afterRender() {
        if (CustomWorld.getPlayer() != null) {
            Camera.setPosition(CustomWorld.getPlayer().getPosition().mul(-1, new Vector2f()));
        }
        //System.out.println(System.currentTimeMillis() - t);
    }

    public static ParticleEmitter getParticleEmitter() {
        return particleEmitter;
    }

    public static Shader getTextShader() {
        return textShader;
    }

    public static Shader getParticleShader() {
        return particleShader;
    }

    @Override
    protected void update(float delta) {
        customUpdater.setDelta(delta);
        new Thread(customUpdater).start();
    }

    @Override
    public void onEvent(EventType eventType) {

    }

    public static Shader getLightningShader() {
        return lightningShader;
    }
}
