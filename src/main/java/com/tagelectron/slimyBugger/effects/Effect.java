package com.tagelectron.slimyBugger.effects;

public class Effect {

    private float dot;

    private EffectType effectType;

    public Effect(float dot, EffectType effectType) {
        this.dot = dot;
        this.effectType = effectType;
    }

    public float getDot() {
        return dot;
    }

    public void setDot(float dot) {
        this.dot = dot;
    }

    public EffectType getEffectType() {
        return effectType;
    }

    public void setEffectType(EffectType effectType) {
        this.effectType = effectType;
    }
}
