package com.tagelectron.slimyBugger.effects;

public enum EffectType {
    Fire, Ice, Poison, Ground
}
