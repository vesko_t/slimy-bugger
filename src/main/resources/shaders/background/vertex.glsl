#version 120

attribute vec2 textures;
attribute vec2 vertices;

varying vec2 texCoords;

void main() {

    texCoords = textures;

    gl_Position = vec4(vertices,0,1);

}
