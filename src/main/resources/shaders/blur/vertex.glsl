#version 120
attribute vec3 vertices;

varying vec2 blurTextureCoordsVertical[11];
varying vec2 blurTextureCoordsHorizontal[11];

uniform vec2 lightsFboResolution;

void main() {

    gl_Position = vec4(vertices, 1.0);
    vec2 centerTexCoords = vertices.xy * 0.5 + 0.5;
    vec2 pixelSize = vec2(1.0 / lightsFboResolution.x, 1.0 / lightsFboResolution.y);

    for(int i = -5; i <= 5; i++){
        blurTextureCoordsHorizontal[i+5] = centerTexCoords + vec2(pixelSize.x * i, 0.0);
        blurTextureCoordsVertical[i+5] = centerTexCoords + vec2(0.0, pixelSize.y * i);
    }
}