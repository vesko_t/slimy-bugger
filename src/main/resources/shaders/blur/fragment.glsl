#version 120
uniform sampler2D lightsTexture;

varying vec2 blurTextureCoordsVertical[11];
varying vec2 blurTextureCoordsHorizontal[11];

void main() {

    vec4 lightSources = vec4(0.0);
    lightSources += texture2D(lightsTexture, blurTextureCoordsHorizontal[0]) * 0.0093;
    lightSources += texture2D(lightsTexture, blurTextureCoordsHorizontal[1]) * 0.028002;
    lightSources += texture2D(lightsTexture, blurTextureCoordsHorizontal[2]) * 0.065984;
    lightSources += texture2D(lightsTexture, blurTextureCoordsHorizontal[3]) * 0.121703;
    lightSources += texture2D(lightsTexture, blurTextureCoordsHorizontal[4]) * 0.175713;
    lightSources += texture2D(lightsTexture, blurTextureCoordsHorizontal[5]) * 0.198596;
    lightSources += texture2D(lightsTexture, blurTextureCoordsHorizontal[6]) * 0.175713;
    lightSources += texture2D(lightsTexture, blurTextureCoordsHorizontal[7]) * 0.121703;
    lightSources += texture2D(lightsTexture, blurTextureCoordsHorizontal[8]) * 0.065984;
    lightSources += texture2D(lightsTexture, blurTextureCoordsHorizontal[9]) * 0.028002;
    lightSources += texture2D(lightsTexture, blurTextureCoordsHorizontal[10]) * 0.0093;
	lightSources += texture2D(lightsTexture, blurTextureCoordsVertical[0]) * 0.0093;
    lightSources += texture2D(lightsTexture, blurTextureCoordsVertical[1]) * 0.028002;
    lightSources += texture2D(lightsTexture, blurTextureCoordsVertical[2]) * 0.065984;
    lightSources += texture2D(lightsTexture, blurTextureCoordsVertical[3]) * 0.121703;
    lightSources += texture2D(lightsTexture, blurTextureCoordsVertical[4]) * 0.175713;
    lightSources += texture2D(lightsTexture, blurTextureCoordsVertical[5]) * 0.198596;
    lightSources += texture2D(lightsTexture, blurTextureCoordsVertical[6]) * 0.175713;
    lightSources += texture2D(lightsTexture, blurTextureCoordsVertical[7]) * 0.121703;
    lightSources += texture2D(lightsTexture, blurTextureCoordsVertical[8]) * 0.065984;
    lightSources += texture2D(lightsTexture, blurTextureCoordsVertical[9]) * 0.028002;
    lightSources += texture2D(lightsTexture, blurTextureCoordsVertical[10]) * 0.0093;

    gl_FragColor = lightSources;
}