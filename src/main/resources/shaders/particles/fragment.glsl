#version 120

uniform sampler2D sampler;

uniform float alpha;

varying vec2 texCoords;

void main() {

    vec4 texture = texture2D(sampler, texCoords);

    texture.a -= alpha;

    gl_FragColor = texture;
}