#version 150

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

varying vec2 texCoords;

uniform mat4 projection;
uniform mat4 model;

void main() {

    vec2 position = gl_in[0].gl_Position.xy;

    gl_Position = projection * model * vec4(position.x - 1, position.y + 1, 0, 1);
    texCoords = vec2(0,0);
    EmitVertex();

    gl_Position = projection * model * vec4(position.x + 1, position.y + 1, 0, 1);
    texCoords = vec2(1,0);
    EmitVertex();

    gl_Position = projection * model * vec4(position.x - 1, position.y - 1, 0, 1);
    texCoords = vec2(0,1);
    EmitVertex();

    gl_Position = projection * model * vec4(position.x + 1, position.y - 1, 0, 1);
    texCoords = vec2(1,1);
    EmitVertex();

    EndPrimitive();
}
