#version 120

uniform sampler2D lightsTexture;
uniform sampler2D sceneTexture;

varying vec2 texCoords;

void main() {
    gl_FragColor = (texture2D(lightsTexture,texCoords) * 1.0) + texture2D(sceneTexture, texCoords);
}
