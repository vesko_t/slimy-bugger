#version 120

attribute vec2 vertices;
attribute vec2 textures;

varying vec2 texCoords;

void main() {
    gl_Position = vec4(vertices, 0.0,1.0);
    texCoords = textures;
}
