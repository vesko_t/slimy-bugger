#version 120

attribute vec2 vertices;
attribute vec2 textures;

uniform mat4 projection;
uniform mat4 model;

varying vec2 texCoords;
void main() {
    texCoords = textures;
    vec4 position = projection * model * vec4(vertices,0,1);
    gl_Position = position;
}