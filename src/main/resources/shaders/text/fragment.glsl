#version 120

uniform sampler2D sampler;

uniform vec3 color;

uniform float opacity;

varying vec2 texCoords;

void main() {

    vec4 texture = texture2D(sampler, texCoords);

    gl_FragColor = vec4(color.rgb * texture.r, texture.r * opacity);

}
