#version 120

uniform sampler2D sampler1;
uniform sampler2D sampler2;
uniform sampler2D sampler3;
uniform sampler2D sampler4;
uniform sampler2D sampler5;
uniform sampler2D sampler6;
uniform sampler2D sampler7;
uniform sampler2D sampler8;
uniform vec4 color = vec4(1,1,1,1);
uniform int samplers;

varying vec2 texCoords;

void main() {

    vec4 textures[8] = vec4[8](
        texture2D(sampler1, texCoords),
        texture2D(sampler2, texCoords),
        texture2D(sampler3, texCoords),
        texture2D(sampler4, texCoords),
        texture2D(sampler5, texCoords),
        texture2D(sampler6, texCoords),
        texture2D(sampler7, texCoords),
        texture2D(sampler8, texCoords)
    );

   /* if(textures[1].a > 0){
        vec3 baseTexture = textures[1].rgb;
        textures[1].rgb += vec3(0.0,0.0,0.5);
        textures[1].rgb *= 1.2;
    }*/

    vec4 finalTexture = textures[0];
    for(int i = 1; i < samplers; i ++){
        if(finalTexture.rgb != vec3(0.0) && textures[i].rgb != vec3(0.0)){
            finalTexture.rgb = textures[i].rgb;
        }else{
            finalTexture += textures[i];
        }
    }
    gl_FragColor = finalTexture * color;


}
