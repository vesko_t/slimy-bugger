#version 120

uniform sampler2D sampler;

varying vec2 texCoords;

void main() {

    vec4 texture = texture2D(sampler, texCoords);

    gl_FragColor = texture;

}
