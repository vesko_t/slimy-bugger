#version 120

attribute vec2 textures;
attribute vec2 vertices;

varying vec2 texCoords;

uniform mat4 projection;
uniform mat4 model;
void main() {

    texCoords = textures;
    vec4 position = projection * model * vec4(vertices,0,1);
    gl_Position = position;
}
