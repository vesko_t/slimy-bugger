#version 120

uniform sampler2D sampler;


varying vec2 texCoords;

void main() {

    vec4 texture = texture2D(sampler, texCoords);

    if(texture.rgb == vec3(1,1,1)){
        texture.a = 0;
    }

    gl_FragColor = texture;


}
