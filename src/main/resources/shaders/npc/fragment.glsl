#version 120

uniform sampler2D sampler;
uniform vec4 color = vec4(1,1,1,1);

varying vec2 texCoords;

void main() {

    gl_FragColor = texture2D(sampler, texCoords) * color;


}
